/*
** albireo.acepansion public API
*/

#ifndef ACEPANSION_H
#define ACEPANSION_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef ACEPANSION_PLUGIN_H
#include <acepansion/plugin.h>
#endif


#ifndef ALBIREO_2ND
#define LIBNAME "albireo.acepansion"
#else
#define LIBNAME "2nd-albireo.acepansion"
#endif
#define VERSION 1
#define REVISION 6
#define DATE "26.02.2024"
#define COPYRIGHT "� 2016-2024 Philippe Rimauro"

#define API_VERSION 7


/*
** Joystick plugin specific API to be called from GUI
*/
VOID Plugin_SetSDCardDrivePath(struct ACEpansionPlugin *plugin, CONST_STRPTR path);
VOID Plugin_SetUSBDrivePath(struct ACEpansionPlugin *plugin, CONST_STRPTR path);


#endif /* ACEPANSION_H */

