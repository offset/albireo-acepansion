/*
** Graphical User Interface using MUI
** for Albireo ACEpansion
*/

#include <clib/alib_protos.h>

#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/muimaster.h>

#include <libraries/asl.h>
#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */



struct Library *IntuitionBase;
struct Library *UtilityBase;
struct Library *MUIMasterBase;



struct MUI_CustomClass *MCC_AlbireoClass = NULL;



/*
** MCC_AlbireoClass definitions
*/
#define AlbireoObject NewObject(MCC_AlbireoClass->mcc_Class, NULL

#define MUIA_Albireo_Plugin (TAG_USER | 0x0000) // [I..] struct ACEpansionPlugin *, Reference to the plugin managing the GUI (mandatory)

#define MUIM_Albireo_Notify (TAG_USER | 0x0100) // struct MUIP_Albireo_Notify, notify the plugin or the GUI about changes
    #define MUIV_Albireo_Notify_ToGUI    0 // Notify the GUI
    #define MUIV_Albireo_Notify_ToPlugin 1 // Notify the plugin
        // For both directions
        #define MUIV_Albireo_Notify_SDCardDrivePath 10 // Notify about SDCard drive path
        #define MUIV_Albireo_Notify_USBDrivePath    11 // Notify about USB drive path

struct MUIP_Albireo_Notify
{
    IPTR id;
    IPTR direction;
    IPTR type;
    CONST_STRPTR path;
};

struct AlbireoData
{
    struct ACEpansionPlugin *plugin;

    Object *STR_SDCardDrivePath;
    Object *STR_USBDrivePath;
};



/*
** Helper functions
*/
static Object * makeDrivePathSelector(CONST_STRPTR help, Object **STR_DrivePath)
{
    return PopaslObject,
        MUIA_Popstring_String, *STR_DrivePath = StringObject,
            StringFrame,
            MUIA_String_SpellChecking, FALSE,
            MUIA_CycleChain, TRUE,
            End,
        MUIA_Popstring_Button, TextObject,
            ButtonFrame,
            MUIA_Weight, 0,
            MUIA_InputMode , MUIV_InputMode_RelVerify,
            MUIA_Background, MUII_ButtonBack,
            MUIA_CycleChain, TRUE,
            MUIA_Text_Contents, "\33I[6:19]", // MUII_PopFile
            End,
        ASLFR_TitleText, GetString(MSG_ASL_REQUEST),
        ASLFR_DrawersOnly, TRUE,
        MUIA_ShortHelp, help,
        End;
}



/*
** MCC_AlbireoClass methods
*/
static IPTR mNotify(struct IClass *cl, Object *obj, struct MUIP_Albireo_Notify *msg)
{
    struct AlbireoData *data = INST_DATA(cl,obj);

    switch(msg->direction)
    {
        case MUIV_Albireo_Notify_ToGUI:
            switch(msg->type)
            {
                case MUIV_Albireo_Notify_SDCardDrivePath:
                    nnset(data->STR_SDCardDrivePath, MUIA_String_Contents, msg->path);
                    break;
                case MUIV_Albireo_Notify_USBDrivePath:
                    nnset(data->STR_USBDrivePath, MUIA_String_Contents, msg->path);
                    break;
            }
            break;

        case MUIV_Albireo_Notify_ToPlugin:
            switch(msg->type)
            {
                case MUIV_Albireo_Notify_SDCardDrivePath:
                    Plugin_SetSDCardDrivePath(data->plugin, msg->path);
                    break;
                case MUIV_Albireo_Notify_USBDrivePath:
                    Plugin_SetUSBDrivePath(data->plugin, msg->path);
                    break;
            }
            break;
    }

    return 0;
}

static IPTR mNew(struct IClass *cl, Object *obj, Msg msg)
{

    struct TagItem *tags,*tag;
    struct AlbireoData *data;

    struct ACEpansionPlugin *plugin = NULL;

    Object *STR_SDCardDrivePath;
    Object *STR_USBDrivePath;

    // Parse initial taglist
    for(tags=((struct opSet *)msg)->ops_AttrList; (tag=NextTagItem(&tags)); )
    {
        switch(tag->ti_Tag)
        {
            case MUIA_Albireo_Plugin:
                plugin = (struct ACEpansionPlugin *)tag->ti_Data;
                break;                              
        }
    }

    if(!plugin) return 0;

    obj = (Object *)DoSuperNew(cl,obj,
        GroupFrameT(GetString(MSG_DRIVES)),
        MUIA_Group_Columns, 2,
        Child, Label2(GetString(MSG_SDCARD_DRIVE)),
        Child, makeDrivePathSelector(GetString(MSG_SDCARD_DRIVE_HELP), &STR_SDCardDrivePath),
        Child, Label2(GetString(MSG_USB_DRIVE)),
        Child, makeDrivePathSelector(GetString(MSG_USB_DRIVE_HELP), &STR_USBDrivePath),
        TAG_MORE,((struct opSet *)msg)->ops_AttrList);

    if(!obj) return 0;

    data = INST_DATA(cl,obj);

    data->plugin = plugin;

    data->STR_SDCardDrivePath = STR_SDCardDrivePath;
    data->STR_USBDrivePath = STR_USBDrivePath;

    DoMethod(STR_SDCardDrivePath, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime,
             obj, 4, MUIM_Albireo_Notify, MUIV_Albireo_Notify_ToPlugin,
                     MUIV_Albireo_Notify_SDCardDrivePath, MUIV_TriggerValue);
    DoMethod(STR_USBDrivePath, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime,
             obj, 4, MUIM_Albireo_Notify, MUIV_Albireo_Notify_ToPlugin,
                     MUIV_Albireo_Notify_USBDrivePath, MUIV_TriggerValue);

    return (IPTR)obj;
}



/*
** MUI Custom Class dispatcher, creation & destruction
*/
DISPATCHER(AlbireoClass)
{
    switch (msg->MethodID)
    {
        case OM_NEW              : return(mNew   (cl,obj,(APTR)msg));
        case MUIM_Albireo_Notify : return(mNotify(cl,obj,(APTR)msg));
    }
}
DISPATCHER_END



/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
    UtilityBase = OpenLibrary("utility.library", 0L);
    IntuitionBase = OpenLibrary("intuition.library", 0L);
    MUIMasterBase = OpenLibrary("muimaster.library", 0L);

    if(UtilityBase && IntuitionBase && MUIMasterBase)
    {
        MCC_AlbireoClass = MUI_CreateCustomClass(
            NULL,
            MUIC_Group,
            NULL,
            sizeof(struct AlbireoData),
            DISPATCHER_REF(AlbireoClass));
    }

    return MCC_AlbireoClass != NULL;
}

/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
    if(MCC_AlbireoClass)
        MUI_DeleteCustomClass(MCC_AlbireoClass);

    CloseLibrary(MUIMasterBase);
    CloseLibrary(IntuitionBase);
    CloseLibrary(UtilityBase);
}



/*
** Function which is called from CreatePlugin() to build the GUI
*/
Object * GUI_Create(struct ACEpansionPlugin *plugin)
{
    return AlbireoObject,
        MUIA_Albireo_Plugin, plugin,
        End;
}

/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
    MUI_DisposeObject(gui);
}



/*
** Functions called from the plugin
*/
VOID GUI_SetSDCardDrivePath(Object *gui, CONST_STRPTR path)
{
    DoMethod(gui, MUIM_Albireo_Notify, MUIV_Albireo_Notify_ToGUI,
             MUIV_Albireo_Notify_SDCardDrivePath, path);
}

VOID GUI_SetUSBDrivePath(Object *gui, CONST_STRPTR path)
{
    DoMethod(gui, MUIM_Albireo_Notify, MUIV_Albireo_Notify_ToGUI,
             MUIV_Albireo_Notify_USBDrivePath, path);
}

