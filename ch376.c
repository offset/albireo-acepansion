/********************************************************************
 *                                                                  *
 * Very minimal CH376 emulation (21.12.2016)                        *
 *                                                                  *
 * Code:                                                            *
 *   J�r�me 'Jede' Debrune                                          *
 *   Philippe 'OffseT' Rimauro                                      *
 *   Christian 'Assinie' Lardi�re                                   *
 *                                                                  *
 ** ch376.c *********************************************************/

//#define DEBUG_CH376

/*
 Changes:

 26.03.2024 - OffseT : Added support for '\' character for root mark
 19.03.2021 - OffseT : Fixed CMD_CHECK_EXIST not to alter command buffer
 08.10.2020 - OffseT : Added support for file size update option at close (implemented for Amiga APIs only)
 14.09.2020 - OffseT : Disabled DOS requesters (Amiga specific feature, do not exist for other implementation)
                       Excluded files with a trailing dot from valid 8.3 files: file with no extension shall not have ending dot (implemented for Amiga APIs only)
 10.09.2020 - OffseT : Added some sanity check to avoid illegal memory accesses
 07.09.2020 - OffseT : Fixed read-only FAT flag to match FIBF_DELETE protection bit in addition to FIBF_WRITE one
                       Fixed delete to properly return failures
 04.08.2020 - OffseT : Fixed and simplified delete and rename code (implemented for Amiga APIs only)
 02.08.2020 - OffseT : Added support for CMD_DIR_INFO_READ, CH376_CMD_DIR_INFO_SAVE and CH376_CMD_WR_OFS_DATA (implemented for Amiga APIs only)
 27.07.2020 - OffseT : Fixed directory scan to also report dummy "." and ".." weird entries of the CH376 (implemented for Amiga APIs only)
 04.06.2020 - OffseT : Fixed CMD_FILE_ERASE for directories, fixed some nasty casting, fixed indentation (there was mixed tabs and spaces)
 07.12.2017 - Assinie: Added support for CMD_GET_FILE_SIZE
 06.12.2017 - Assinie: Added support for CMD_DISK_CAPACITY
 13.11.2017 - Assinie: Improve '*' wildcard support for CMD_FILE_OPEN
 05.10.2017 - Assinie: Added support for CMD_DIR_CREATE and CMD_FILE_ERASE (Linux only)
 21.08.2017 - Jede   : Added support for CMD_DIR_CREATE and CMD_FILE_ERASE (WIN32 only)
 22.07.2017 - OffseT : Added support for CMD_DIR_CREATE and CMD_FILE_ERASE (Added related Amiga system APIs only)
 */

/* /// "Includes" */

#include <string.h>

#include "ch376.h"

#if defined(__MORPHOS__) || defined (__AMIGA__) || defined (__AROS__)

#ifdef DEBUG_CH376
#include <clib/debug_protos.h>
#define dbg_printf kprintf
#else
#define dbg_printf(...)
#endif

#elif defined(WIN32)

#ifdef DEBUG_CH376
static void dbg_printf(LPCTSTR str, ...)
{
        TCHAR buffer[256];

        va_list args;
        va_start(args, str);
        _vsnprintf_s(buffer, sizeof(buffer) - 1, sizeof(buffer) - 1, str, args);
        OutputDebugString(buffer);
        va_end(args);
}
#else
#define dbg_printf(...)
#endif

#elif defined(__unix__) || defined(__APPLE__) || defined(__HAIKU__)

#ifdef DEBUG_CH376
#define dbg_printf(...) fprintf(stderr, __VA_ARGS__)
#else
#define dbg_printf(...)
#endif

#else
#error "FixMe!"
#endif

/* /// */

/* /// "CH376 interface commands and constants" */

// Chip version
#define CH376_DATA_IC_VER 3

// Commands
#define CH376_CMD_NONE          0x00
#define CH376_CMD_GET_IC_VER    0x01
#define CH376_CMD_CHECK_EXIST   0x06
#define CH376_CMD_GET_FILE_SIZE 0x0c
#define CH376_CMD_SET_USB_MODE  0x15
#define CH376_CMD_GET_STATUS    0x22
#define CH376_CMD_RD_USB_DATA0  0x27
#define CH376_CMD_WR_REQ_DATA   0x2d
#define CH376_CMD_WR_OFS_DATA   0x2e
#define CH376_CMD_SET_FILE_NAME 0x2f
#define CH376_CMD_DISK_MOUNT    0x31
#define CH376_CMD_FILE_OPEN     0x32
#define CH376_CMD_FILE_ENUM_GO  0x33
#define CH376_CMD_FILE_CREATE   0x34
#define CH376_CMD_FILE_ERASE    0x35
#define CH376_CMD_FILE_CLOSE    0x36
#define CH376_CMD_DIR_INFO_READ 0x37
#define CH376_CMD_DIR_INFO_SAVE 0x38
#define CH376_CMD_BYTE_LOCATE   0x39
#define CH376_CMD_BYTE_READ     0x3a
#define CH376_CMD_BYTE_RD_GO    0x3b
#define CH376_CMD_BYTE_WRITE    0x3c
#define CH376_CMD_BYTE_WR_GO    0x3d
#define CH376_CMD_DISK_CAPACITY 0x3e
#define CH376_CMD_DISK_QUERY    0x3f
#define CH376_CMD_DIR_CREATE    0x40
#define CH376_CMD_DISK_RD_GO    0x55

#define CH376_ARG_SET_USB_MODE_INVALID  0x00
#define CH376_ARG_SET_USB_MODE_SD_HOST  0x03
#define CH376_ARG_SET_USB_MODE_USB_HOST 0x06

// Status & errors
#define CH376_ERR_OPEN_DIR   0x41
#define CH376_ERR_MISS_FILE  0x42
#define CH376_ERR_FOUND_NAME 0x43

#define CH376_RET_SUCCESS 0x51
#define CH376_RET_ABORT   0x5f

#define CH376_INT_SUCCESS    0x14
#define CH376_INT_DISK_READ  0x1d
#define CH376_INT_DISK_WRITE 0x1e

/* /// */

/* /// "CH376 data structures" */

#define CMD_DATA_REQ_SIZE 0xff

#pragma pack(1)

// Important: All values in all structures are stored in little-endian (either in 16 or 32 bit)

struct MountInfo
{
    DOS_U8   MOUNT_DeviceType;
    DOS_U8   MOUNT_RemovableMedia;
    DOS_U8   MOUNT_Versions;
    DOS_U8   MOUNT_DataFormatAndEtc;
    DOS_U8   MOUNT_AdditionalLength;
    DOS_U8   MOUNT_Reserved1;
    DOS_U8   MOUNT_Reserved2;
    DOS_U8   MOUNT_MiscFlag;
    DOS_TEXT MOUNT_VendorIdStr[8];
    DOS_TEXT MOUNT_ProductIdStr[16];
    DOS_TEXT MOUNT_ProductRevStr[4];
};

struct DataWrite
{
    struct FatDirInfo WR_FatDirInfo;
    DOS_BOOL          WR_Configured;
    DOS_U8            WR_Offset;
    DOS_U8            WR_Length;
};

union CommandData
{
    DOS_TEXT          CMD_FileName[14];     // [W/O] CH376_CMD_SET_FILE_NAME
    struct MountInfo  CMD_MountInfo;        // [R/O] CH376_CMD_DISK_MOUNT
    struct FatDirInfo CMD_FatDirInfo;       // [R/O] CH376_CMD_FILE_OPEN, CH376_CMD_FILE_ENUM_GO
    struct DiskQuery  CMD_DiskQuery;        // [R/O] CH376_CMD_DISK_QUERY, CH376_CMD_DISK_CAPACITY
    struct DataWrite  CMD_DataWrite;        // [R/O] CH376_CMD_WR_OFS_DATA
    DOS_U8            CMD_FileSeek[4];      // [R/W] CH376_CMD_BYTE_LOCATE
    DOS_U8            CMD_FileReadWrite[2]; // [W/O] CH376_CMD_BYTE_READ, CH376_CMD_BYTE_WRITE
    DOS_U8            CMD_IOBuffer[255];    // [R/W] CH376_CMD_BYTE_READ, CH376_CMD_BYTE_RD_GO, CH376_CMD_BYTE_WRITE, CH376_CMD_BYTE_WR_GO
    DOS_U8            CMD_FileSize[4];      // [R/W] CH376_CMD_GET_FILE_SIZE, CH376_CMD_SET_FILE_SIZE
};

#pragma pack()

/* /// */

/* /// "CH376 runtime structure" */

struct ch376
{
    DOS_CONTEXT context;

    DOS_U8 command;
    DOS_U8 command_status;

    DOS_U8 interface_status;
    DOS_U8 usb_mode;

    DOS_U8 check_byte;

    union CommandData cmd_data;
    DOS_U8 nb_bytes_in_cmd_data;
    DOS_U8 pos_rw_in_cmd_data;
    DOS_U8 buffer_read_count;

    DOS_U16 bytes_to_read_write;
    DOS_BOOL bytes_were_written;

    DOS_LOCK root_dir_lock;
    DOS_LOCK current_dir_lock;
    DOS_FILE current_file;
    DOS_DIR current_directory_browsing;

    DOS_TEXT *sdcard_drive_path;
    DOS_TEXT *usb_drive_path;
};

/* /// */

/* /// "CH376 private prototypes" */

static void clear_structure(struct ch376 *ch376);
static void cancel_all_io(struct ch376 *ch376);
static void file_read_chunk(struct ch376 *ch376);
static void file_write_chunk(struct ch376 *ch376);

/* /// */

/* /// "CH376 private subroutines" */

static void clear_structure(struct ch376 *ch376)
{
    ch376->command = CH376_CMD_NONE;
    ch376->command_status = 0;
    ch376->interface_status = 0;
    ch376->nb_bytes_in_cmd_data = 0;
    ch376->pos_rw_in_cmd_data = 0;
    ch376->usb_mode = CH376_ARG_SET_USB_MODE_INVALID;
    ch376->bytes_to_read_write = 0;
    ch376->root_dir_lock = (DOS_LOCK)0;
    ch376->current_dir_lock = (DOS_LOCK)0;
    ch376->current_file = (DOS_FILE)0;
    ch376->current_directory_browsing = (DOS_DIR)0;
}

static void cancel_all_io(struct ch376 *ch376)
{
    // Cancel data buffer read/write sessions
    ch376->bytes_to_read_write = 0;

    // End directory browing or file i/o sessions
    system_finish_examine_directory(&ch376->context, ch376->current_directory_browsing);
    ch376->current_directory_browsing = (DOS_DIR)0;
    system_file_close(&ch376->context, ch376->current_file);
    ch376->current_file = (DOS_FILE)0;

    // Release potential already obtained lock for root and current
    system_release_directory_lock(&ch376->context, ch376->root_dir_lock);
    ch376->root_dir_lock = (DOS_LOCK)0;
    system_release_directory_lock(&ch376->context, ch376->current_dir_lock);
    ch376->current_dir_lock = (DOS_LOCK)0;
}

static void file_read_chunk(struct ch376 *ch376)
{
    DOS_S32 bytes_to_read_now;
    DOS_S32 bytes_actually_read;

    if(ch376->bytes_to_read_write > sizeof(ch376->cmd_data.CMD_IOBuffer))
        bytes_to_read_now = sizeof(ch376->cmd_data.CMD_IOBuffer);
    else
        bytes_to_read_now = ch376->bytes_to_read_write;

    ch376->buffer_read_count = (ch376->buffer_read_count + 1) % 0x03;
    if (!ch376->buffer_read_count && bytes_to_read_now > 2)
        bytes_to_read_now = 2;

    dbg_printf("\n*** read count/ %d\n", ch376->buffer_read_count);
    bytes_actually_read = system_file_read(&ch376->context, ch376->current_file, &ch376->cmd_data.CMD_IOBuffer, bytes_to_read_now);

    if(bytes_actually_read >= 0)
    {
        ch376->bytes_to_read_write -= (DOS_U16)bytes_actually_read;

        ch376->nb_bytes_in_cmd_data = (DOS_U8)bytes_actually_read;
        ch376->interface_status = 127;
        // CH376 specification tells that the last buffer is returned with INT_SUCCESS
        // but it is not the case (at least with revision 4). An empty buffer is always
        // returned with INT_SUCCESS. As a consequence some routines do not read the last
        // buffer when INT_SUCCESS is returned. It is not correct regarding CH376
        // specification, but to keep compatibility with these (wrong) routines,
        // we also always return an empty buffer with INT_SUCCESS by checking
        // bytes_actually_read == 0 instead of bytes_actually_read < bytes_to_read_now
        if(bytes_to_read_now == 0 || bytes_actually_read == 0)
        {
            dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_READ] all done: read operated with success (no more data)\n");
            ch376->command_status = CH376_INT_SUCCESS;
        }
        else
        {
            dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_READ] all done: read operated with success (waiting data read for %d bytes out of %d)\n", ch376->nb_bytes_in_cmd_data, ch376->bytes_to_read_write);
            ch376->command_status = CH376_INT_DISK_READ;
        }
    }
    else
    {
        dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_READ] aborted: read failure\n");

        ch376->nb_bytes_in_cmd_data = 0;
        ch376->interface_status = 0;
        ch376->command_status = CH376_RET_ABORT;
    }
}

static void file_write_chunk(struct ch376 *ch376)
{
    DOS_S32 bytes_to_write_now;
    DOS_S32 bytes_actually_written;

    bytes_to_write_now = ch376->nb_bytes_in_cmd_data;

    bytes_actually_written = system_file_write(&ch376->context, ch376->current_file, &ch376->cmd_data.CMD_IOBuffer, bytes_to_write_now);

    if(bytes_actually_written == bytes_to_write_now)
    {
        ch376->bytes_to_read_write -= (DOS_U16)bytes_actually_written;

        ch376->nb_bytes_in_cmd_data = 0;
        ch376->interface_status = 127;
        if(ch376->bytes_to_read_write == 0)
        {
            dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_WRITE] all done: write operated with success (no more data)\n");
            ch376->command_status = CH376_INT_SUCCESS;
        }
        else
        {
            if(ch376->bytes_to_read_write > sizeof(ch376->cmd_data.CMD_IOBuffer))
                ch376->nb_bytes_in_cmd_data = sizeof(ch376->cmd_data.CMD_IOBuffer);
            else
                ch376->nb_bytes_in_cmd_data = (DOS_U8)ch376->bytes_to_read_write;

            dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_WRITE] all done: write operated with success (waiting data write for %d bytes out of %d)\n", ch376->nb_bytes_in_cmd_data, ch376->bytes_to_read_write);
            ch376->command_status = CH376_INT_DISK_WRITE;
        }
    }
    else
    {
        dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_WRITE] aborted: write failure: write %d, written %d\n", bytes_to_write_now, bytes_actually_written);

        ch376->nb_bytes_in_cmd_data = 0;
        ch376->interface_status = 0;
        ch376->command_status = CH376_RET_ABORT;
    }
}

/* /// */

/* /// "CH376 public read command port" */

DOS_U8 ch376_read_command_port(struct ch376 *ch376)
{
    dbg_printf("[READ][COMMAND] during command &%02x status &%02x\n", ch376->command, ch376->command_status);
    return ch376->interface_status;
}

/* /// */

/* /// "CH376 public read data port" */

DOS_U8 ch376_read_data_port(struct ch376 *ch376)
{
    DOS_U8 data_out = 0xff; // Hi-Z

    dbg_printf(">> [READ][DATA] for during command &%02x status &%02x\n", ch376->command, ch376->command_status);

    switch(ch376->command)
    {
    case CH376_CMD_CHECK_EXIST:
        data_out = ch376->check_byte;
        dbg_printf("[READ][DATA][CH376_CMD_CHECK_EXIST] setting data port to &%02x\n", data_out);
        break;

    case CH376_CMD_GET_FILE_SIZE:
        if(ch376->nb_bytes_in_cmd_data)
        {
            if(ch376->nb_bytes_in_cmd_data != ch376->pos_rw_in_cmd_data)
            {
                data_out = ch376->cmd_data.CMD_FileSize[ch376->pos_rw_in_cmd_data];

                dbg_printf("[READ][DATA][CH376_CMD_GET_FILE_SIZE] read &%02x from i/o buffer at position &%02x\n", data_out, ch376->pos_rw_in_cmd_data);

                if(++ch376->pos_rw_in_cmd_data == ch376->nb_bytes_in_cmd_data)
                    ch376->nb_bytes_in_cmd_data = 0;
            }
        }
        else
        {
            data_out = 0;
            dbg_printf("[READ][DATA][CH376_CMD_GET_FILE_SIZE] nothing to read from i/o buffer\n");
        }

        break;

    case CH376_CMD_GET_IC_VER:
        data_out = (0x40 | CH376_DATA_IC_VER) & 0x7f;
        dbg_printf("[READ][DATA][CH376_CMD_GET_IC_VER] setting data port to &%02x\n", data_out);
        break;

    case CH376_CMD_SET_USB_MODE:
        if(ch376->usb_mode == CH376_ARG_SET_USB_MODE_INVALID)
        {
            data_out = CH376_RET_ABORT;
            dbg_printf("[READ][DATA][CH376_SET_USB_MODE] aborted!\n");
        }
        else
        {
            data_out = CH376_RET_SUCCESS;
            dbg_printf("[READ][DATA][CH376_SET_USB_MODE] completed!\n");
        }
        break;

    case CH376_CMD_GET_STATUS:
        data_out = ch376->command_status;
        dbg_printf("[READ][DATA][CH376_CMD_GET_STATUS] setting data port to &%02x\n", data_out);
        break;

    case CH376_CMD_RD_USB_DATA0:
        if(ch376->nb_bytes_in_cmd_data)
        {
            if(ch376->pos_rw_in_cmd_data == CMD_DATA_REQ_SIZE)
            {
                data_out = ch376->nb_bytes_in_cmd_data;
                ch376->pos_rw_in_cmd_data = 0;
                dbg_printf("[READ][DATA][CH376_CMD_RD_USB_DATA0] read i/o buffer size: &%02x\n", data_out);
            }
            else if(ch376->nb_bytes_in_cmd_data != ch376->pos_rw_in_cmd_data)
            {
                data_out = ch376->cmd_data.CMD_IOBuffer[ch376->pos_rw_in_cmd_data];

                dbg_printf("[READ][DATA][CH376_CMD_RD_USB_DATA0] read \"%c\" (&%02x) from i/o buffer at position &%02x\n", data_out, data_out, ch376->pos_rw_in_cmd_data);

                if(++ch376->pos_rw_in_cmd_data == ch376->nb_bytes_in_cmd_data)
                    ch376->nb_bytes_in_cmd_data = 0;
            }
        }
        else
        {
            data_out = 0;
            dbg_printf("[READ][DATA][CH376_CMD_RD_USB_DATA0] nothing to read from i/o buffer\n");
        }
        break;

    case CH376_CMD_WR_REQ_DATA:
        if(ch376->nb_bytes_in_cmd_data)
        {
            if(ch376->pos_rw_in_cmd_data == CMD_DATA_REQ_SIZE)
            {
                data_out = ch376->nb_bytes_in_cmd_data;
                ch376->pos_rw_in_cmd_data = 0;
                dbg_printf("[READ][DATA][CH376_CMD_WR_REQ_DATA] read i/o buffer size: &%02x\n", data_out);
            }
        }
        else
        {
            data_out = 0;
            dbg_printf("[READ][DATA][CH376_CMD_WR_REQ_DATA] nothing to read from i/o buffer\n");
        }
        break;

    // Emulate CH376 bug which returns the 1st byte in data buffer
    // when read is performed on an unexpected command
    default:
        data_out = ch376->cmd_data.CMD_IOBuffer[0];
        break;
    }

    dbg_printf("<< [READ][DATA] for during command &%02x status &%02x\n", ch376->command, ch376->command_status);


  return data_out;
}

/* /// */

/* /// "CH376 public write command port" */

void ch376_write_command_port(struct ch376 *ch376, DOS_U8 command)
{
    dbg_printf(">> [WRITE][COMMAND] Write command &%02x status &%02x\n", command, ch376->command_status);

    ch376->interface_status = 0;

    // Emulate CH376 bug which can get the check byte
    // from the command port instead of the data port!
    if(ch376->command == CH376_CMD_CHECK_EXIST)
    {
        ch376->check_byte = ~command;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_CHECK_EXIST] got check byte &%02x from command port!\n", command);
    }

    switch(command)
    {
    case CH376_CMD_CHECK_EXIST:
        ch376->command = CH376_CMD_CHECK_EXIST;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_CHECK_EXIST] waiting for check byte\n");
        break;

    case CH376_CMD_GET_FILE_SIZE:
        ch376->command = CH376_CMD_GET_FILE_SIZE;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_GET_FILE_SIZE] wait for &68 byte\n");
        break;

    case CH376_CMD_GET_IC_VER:
        ch376->command = CH376_CMD_GET_IC_VER;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_GET_IC_VER]\n");
        break;

    case CH376_CMD_SET_USB_MODE:
        ch376->command = CH376_CMD_SET_USB_MODE;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_SET_USB_MODE] waiting for usb mode data\n");
        break;

    case CH376_CMD_DISK_MOUNT:
        ch376->command = CH376_CMD_DISK_MOUNT;
        cancel_all_io(ch376);
        // If directory is available, we consider that it's mounted!
        if(ch376->usb_mode == CH376_ARG_SET_USB_MODE_SD_HOST)
        {
            ch376->root_dir_lock = system_obtain_directory_lock(&ch376->context, ch376->sdcard_drive_path, (DOS_LOCK)0);
            ch376->current_dir_lock = system_clone_directory_lock(&ch376->context, ch376->root_dir_lock);
        }
        else if(ch376->usb_mode == CH376_ARG_SET_USB_MODE_USB_HOST)
        {
            ch376->root_dir_lock = system_obtain_directory_lock(&ch376->context, ch376->usb_drive_path, (DOS_LOCK)0);
            ch376->current_dir_lock = system_clone_directory_lock(&ch376->context, ch376->root_dir_lock);
        }

        if(ch376->root_dir_lock)
        {
            ch376->cmd_data.CMD_MountInfo.MOUNT_DeviceType = 0;
            ch376->cmd_data.CMD_MountInfo.MOUNT_RemovableMedia = 0;
            ch376->cmd_data.CMD_MountInfo.MOUNT_Versions = 0;
            ch376->cmd_data.CMD_MountInfo.MOUNT_DataFormatAndEtc = 0;
            ch376->cmd_data.CMD_MountInfo.MOUNT_AdditionalLength = 0;
            ch376->cmd_data.CMD_MountInfo.MOUNT_Reserved1 = 0;
            ch376->cmd_data.CMD_MountInfo.MOUNT_Reserved2 = 0;
            ch376->cmd_data.CMD_MountInfo.MOUNT_MiscFlag = 0;
            ch376->cmd_data.CMD_MountInfo.MOUNT_VendorIdStr[0] = 'J';
            ch376->cmd_data.CMD_MountInfo.MOUNT_VendorIdStr[1] = 'E';
            ch376->cmd_data.CMD_MountInfo.MOUNT_VendorIdStr[2] = '+';
            ch376->cmd_data.CMD_MountInfo.MOUNT_VendorIdStr[3] = 'O';
            ch376->cmd_data.CMD_MountInfo.MOUNT_VendorIdStr[4] = 'F';
            ch376->cmd_data.CMD_MountInfo.MOUNT_VendorIdStr[5] = '+';
            ch376->cmd_data.CMD_MountInfo.MOUNT_VendorIdStr[6] = 'A';
            ch376->cmd_data.CMD_MountInfo.MOUNT_VendorIdStr[7] = 'S';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 0] = 'C';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 1] = 'H';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 2] = '3';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 3] = '7';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 4] = '6';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 5] = ' ';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 6] = 'E';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 7] = 'M';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 8] = 'U';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[ 9] = 'L';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[10] = 'A';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[11] = 'T';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[12] = 'O';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[13] = 'R';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[14] = ' ';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductIdStr[15] = ' ';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductRevStr[0] = '0';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductRevStr[1] = '1';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductRevStr[2] = '0';
            ch376->cmd_data.CMD_MountInfo.MOUNT_ProductRevStr[3] = '0';

            dbg_printf("[WRITE][COMMAND][CH376_CMD_DISK_MOUNT] drive directory is found (mounted :); additional data available\n");

            ch376->nb_bytes_in_cmd_data = sizeof(ch376->cmd_data.CMD_MountInfo);
            ch376->interface_status = 127; // Found :)
            ch376->command_status = CH376_INT_SUCCESS;
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_DISK_MOUNT] drive directory not found\n");
            ch376->command_status = 0x1f; // Don't know if it's this code
        }
        break;

    case CH376_CMD_GET_STATUS:
        ch376->command = CH376_CMD_GET_STATUS;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_GET_STATUS] waiting for status reading\n");
        break;

    case CH376_CMD_SET_FILE_NAME:
        ch376->command = CH376_CMD_SET_FILE_NAME;
        ch376->pos_rw_in_cmd_data = 0;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_SET_FILE_NAME] waiting for file name\n");
        break;

    case CH376_CMD_FILE_OPEN:
        ch376->command = CH376_CMD_FILE_OPEN;
        // mounted?
        if(ch376->root_dir_lock)
        {
            int i = 0;

            ch376->bytes_were_written = DOS_FALSE;

            // back to root?
            if(ch376->cmd_data.CMD_FileName[i] == '/' || ch376->cmd_data.CMD_FileName[i] == '\\')
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_OPEN] opening root directory\n");
                system_release_directory_lock(&ch376->context, ch376->current_dir_lock);
                ch376->current_dir_lock = system_clone_directory_lock(&ch376->context, ch376->root_dir_lock);
                i++;
            }

            if(ch376->cmd_data.CMD_FileName[i] == '\0')
            {
                // We are done
                ch376->interface_status = 127; // Found :)
                ch376->command_status = CH376_ERR_OPEN_DIR;
            }
            else
            {
                // Cancel any ongoing examine
                system_finish_examine_directory(&ch376->context, ch376->current_directory_browsing);
                ch376->current_directory_browsing = (DOS_DIR)0;

                // wildcard?
                if(strchr(ch376->cmd_data.CMD_FileName,'*') || strchr(ch376->cmd_data.CMD_FileName,'?'))
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_OPEN] examining directory contents\n");

                    // Start a directory examine session
                    ch376->current_directory_browsing = system_start_examine_directory(&ch376->context, ch376->current_dir_lock, ch376->cmd_data.CMD_FileName);
                    goto file_enum_go;
                }
                else
                {
                    DOS_TEXT fixed_file_name[13]; // Max = 8 + '.' + 3 + '\0'
                    DOS_LOCK current_dir_lock;
                    system_trim_file_name(&ch376->cmd_data.CMD_FileName[i], fixed_file_name);

                    current_dir_lock = system_obtain_directory_lock(&ch376->context, fixed_file_name, ch376->current_dir_lock);

                    // Enter new directory?
                    if(current_dir_lock)
                    {
                        dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_OPEN] entering directory: %s\n", &ch376->cmd_data.CMD_FileName[i]);

                        system_release_directory_lock(&ch376->context, ch376->current_dir_lock);
                        ch376->current_dir_lock = current_dir_lock;

                        ch376->interface_status = 127; // Found :)
                        ch376->command_status = CH376_ERR_OPEN_DIR;
                    }
                    // Open existing file?
                    else
                    {
                        system_file_close(&ch376->context, ch376->current_file);
                        ch376->current_file = system_file_open_existing(&ch376->context, fixed_file_name, ch376->current_dir_lock);

                        dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_OPEN] opening the file: %s\n", &ch376->cmd_data.CMD_FileName[i]);

                        if(ch376->current_file)
                        {
                            ch376->interface_status = 127; // Found :)
                            ch376->command_status = CH376_INT_SUCCESS;
                            // ch376->buffer_read_count = 0; // Init read buffer count (also needed when opendir?)
                        }
                        else
                        {
                            ch376->interface_status = 0;
                            ch376->command_status = CH376_ERR_MISS_FILE;
                        }
                    }
                }
            }
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_OPEN] no operation possible (device not mounted)\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    case CH376_CMD_FILE_CREATE:
        ch376->command = CH376_CMD_FILE_CREATE;
        // mounted?
        if(ch376->root_dir_lock)
        {
            int i = 0;

            // back to root?
            if(ch376->cmd_data.CMD_FileName[i] == '/' || ch376->cmd_data.CMD_FileName[i] == '\\')
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_CREATE] opening root directory\n");
                system_release_directory_lock(&ch376->context, ch376->current_dir_lock);
                ch376->current_dir_lock = system_clone_directory_lock(&ch376->context, ch376->root_dir_lock);
                i++;
            }

            if(ch376->cmd_data.CMD_FileName[i] == '\0')
            {
                // We are done
                ch376->interface_status = 127; // Found :)
                ch376->command_status = CH376_INT_SUCCESS;
            }
            else
            {
                DOS_TEXT fixed_file_name[13]; // Max = 8 + '.' + 3 + '\0'
                system_trim_file_name(&ch376->cmd_data.CMD_FileName[i], fixed_file_name);

                // Create file
                system_file_close(&ch376->context, ch376->current_file);
                ch376->current_file = system_file_open_new(&ch376->context, fixed_file_name, ch376->current_dir_lock);

                dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_CREATE] creating the file: %s\n", &ch376->cmd_data.CMD_FileName[i]);

                if(ch376->current_file)
                {
                    ch376->interface_status = 127; // Found :)
                    ch376->command_status = CH376_INT_SUCCESS;
                }
                else
                {
                    ch376->interface_status = 0;
                    ch376->command_status = CH376_ERR_MISS_FILE;
                }
            }
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_CREATE] no operation possible (device not mounted)\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    case CH376_CMD_DIR_CREATE:
        ch376->command = CH376_CMD_DIR_CREATE;
        // mounted?
        if(ch376->root_dir_lock)
        {
            DOS_TEXT fixed_file_name[13]; // Max = 8 + '.' + 3 + '\0'
            DOS_FILE existing_file;
            int i = 0;

            // back to root?
            if(ch376->cmd_data.CMD_FileName[i] == '/' || ch376->cmd_data.CMD_FileName[i] == '\\')
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_CREATE] opening root directory\n");
                system_release_directory_lock(&ch376->context, ch376->current_dir_lock);
                ch376->current_dir_lock = system_clone_directory_lock(&ch376->context, ch376->root_dir_lock);
                i++;
            }

            system_trim_file_name(&ch376->cmd_data.CMD_FileName[i], fixed_file_name);

            existing_file = system_file_open_existing(&ch376->context, fixed_file_name, ch376->current_dir_lock);

            if(existing_file)
            {
                system_file_close(&ch376->context, existing_file);
                ch376->interface_status = 0;
                ch376->command_status = CH376_ERR_FOUND_NAME;
            }
            else
            {
                DOS_LOCK created_dir_lock;

                // Already created?
                created_dir_lock = system_obtain_directory_lock(&ch376->context, fixed_file_name, ch376->current_dir_lock);

                if (!created_dir_lock)
                    created_dir_lock = system_create_directory(&ch376->context, fixed_file_name, ch376->current_dir_lock);

                // Actually created?
                if(created_dir_lock)
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_CREATE] entering created directory: %s\n", &ch376->cmd_data.CMD_FileName[i]);

                    system_release_directory_lock(&ch376->context, ch376->current_dir_lock);
                    ch376->current_dir_lock = created_dir_lock;

                    ch376->interface_status = 127;
                    ch376->command_status = CH376_INT_SUCCESS;
                }
                else
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_CREATE] directory could not be created: %s\n", &ch376->cmd_data.CMD_FileName[i]);

                    ch376->interface_status = 0;
                    ch376->command_status = CH376_ERR_MISS_FILE;
                }
            }
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_CREATE] no operation possible (device not mounted)\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    case CH376_CMD_FILE_ERASE:
        ch376->command = CH376_CMD_FILE_ERASE;
        // mounted?
        if(ch376->root_dir_lock)
        {
            if(ch376->current_file)
            {
                if(system_file_delete(&ch376->context, ch376->current_file))
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_ERASE] file deleted\n");

                    ch376->interface_status = 127;
                    ch376->command_status = CH376_INT_SUCCESS;
                }
                else
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_ERASE] file could not be deleted\n");

                    ch376->interface_status = 0;
                    ch376->command_status = CH376_RET_ABORT;
                }

                // success or not, current_file is not valid anymore
                ch376->current_file = (DOS_FILE)0;
            }
            else if(!system_is_root_dir(&ch376->context, ch376->current_dir_lock, ch376->root_dir_lock))
            {
                if(system_directory_delete(&ch376->context, ch376->current_dir_lock))
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_ERASE] directory deleted\n");

                    ch376->interface_status = 127;
                    ch376->command_status = CH376_INT_SUCCESS;
                }
                else
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_ERASE] directory could not be deleted\n");

                    ch376->interface_status = 0;
                    ch376->command_status = CH376_RET_ABORT;
                }

                // success or not, current_dir_lock is not valid anymore, go back to root by default
                ch376->current_dir_lock = system_clone_directory_lock(&ch376->context, ch376->root_dir_lock);
            }
            else
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_ERASE] no operation possible (file or directory not opened)\n");

                ch376->interface_status = 0;
                ch376->command_status = CH376_ERR_MISS_FILE;
            }
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_ERASE] no operation possible (device not mounted)\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    case CH376_CMD_RD_USB_DATA0:
        ch376->command = CH376_CMD_RD_USB_DATA0;
        ch376->pos_rw_in_cmd_data = CMD_DATA_REQ_SIZE; // Will be reset when size is sent
        dbg_printf("[WRITE][COMMAND][CH376_CMD_RD_USB_DATA0] waiting for i/o buffer read\n");
        break;

    case CH376_CMD_WR_REQ_DATA:
        ch376->command = CH376_CMD_WR_REQ_DATA;
        ch376->pos_rw_in_cmd_data = CMD_DATA_REQ_SIZE; // Will be reset when size is sent
        dbg_printf("[WRITE][COMMAND][CH376_CMD_WR_REQ_DATA] waiting for i/o buffer write\n");
        break;

    case CH376_CMD_FILE_ENUM_GO:
        ch376->command = CH376_CMD_FILE_ENUM_GO;
file_enum_go:
        if(system_go_examine_directory(&ch376->context, ch376->current_dir_lock, ch376->current_directory_browsing, &ch376->cmd_data.CMD_FatDirInfo))
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_ENUM_GO] next directory entry in buffer\n");
            ch376->nb_bytes_in_cmd_data = sizeof(ch376->cmd_data.CMD_FatDirInfo);
            ch376->interface_status = 127;
            ch376->command_status = CH376_INT_DISK_READ;
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_ENUM_GO] directory browing finished\n");

            system_finish_examine_directory(&ch376->context, ch376->current_directory_browsing);
            ch376->current_directory_browsing = (DOS_DIR)0;

            ch376->interface_status = 0;
            ch376->command_status = CH376_ERR_MISS_FILE;
        }
        break;

    case CH376_CMD_DISK_CAPACITY:
        ch376->command = CH376_CMD_DISK_CAPACITY;
        // mounted?
        if(ch376->root_dir_lock)
        {
            if(system_get_disk_info(&ch376->context, ch376->root_dir_lock, &ch376->cmd_data.CMD_DiskQuery))
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_DISK_CAPACITY] sector capacity in buffer\n");
                // Only first 4 bytes of cmd_data.CMD_DiskQuery
                ch376->nb_bytes_in_cmd_data = 4;
                ch376->interface_status = 127;
                ch376->command_status = CH376_INT_SUCCESS;
            }
            else
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_DISK_CAPACITY] no operation possible (device not mounted)\n");
                ch376->interface_status = 0;
                ch376->command_status = CH376_RET_ABORT;
            }
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_DISK_CAPACITY] no operation possible (device not mounted)\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    case CH376_CMD_DISK_QUERY:
        ch376->command = CH376_CMD_DISK_QUERY;
        // mounted?
        if(ch376->root_dir_lock)
        {
            if(system_get_disk_info(&ch376->context, ch376->root_dir_lock, &ch376->cmd_data.CMD_DiskQuery))
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_DISK_QUERY] sector capacity in buffer\n");
                ch376->nb_bytes_in_cmd_data = sizeof(ch376->cmd_data.CMD_DiskQuery);
                ch376->interface_status = 127;
                ch376->command_status = CH376_INT_SUCCESS;
            }
            else
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_DISK_QUERY] no operation possible (device not mounted)\n");
                ch376->interface_status = 0;
                ch376->command_status = CH376_RET_ABORT;
            }
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_DISK_QUERY] no operation possible (device not mounted)\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    case CH376_CMD_BYTE_LOCATE:
        ch376->command = CH376_CMD_BYTE_LOCATE;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_BYTE_LOCATE] waiting for seek position\n");
        ch376->pos_rw_in_cmd_data = 0;
        break;

    case CH376_CMD_BYTE_READ:
        ch376->command = CH376_CMD_BYTE_READ;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_BYTE_READ] waiting for read size\n");
        ch376->pos_rw_in_cmd_data = 0;
        break;

    case CH376_CMD_BYTE_RD_GO:
        ch376->command = CH376_CMD_BYTE_RD_GO;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_BYTE_RD_GO] waiting for next chunk to read\n");
        file_read_chunk(ch376);
        break;

    case CH376_CMD_BYTE_WRITE:
        ch376->command = CH376_CMD_BYTE_WRITE;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_BYTE_WRITE] waiting for write size\n");
        ch376->pos_rw_in_cmd_data = 0;
        break;

    case CH376_CMD_BYTE_WR_GO:
        ch376->command = CH376_CMD_BYTE_WR_GO;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_BYTE_WR_GO] waiting for next chunk to write\n");
        file_write_chunk(ch376);
        ch376->bytes_were_written = DOS_TRUE;
        break;

    case CH376_CMD_FILE_CLOSE:
        ch376->command = CH376_CMD_FILE_CLOSE;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_CLOSE] waiting for close mode\n");
        break;

    case CH376_CMD_WR_OFS_DATA:
        ch376->command = CH376_CMD_WR_OFS_DATA;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_WR_OFS_DATA] waiting for write offset and size\n");
        ch376->pos_rw_in_cmd_data = 0;
        ch376->cmd_data.CMD_DataWrite.WR_Configured = DOS_FALSE;
        break;

    case CH376_CMD_DIR_INFO_READ:
        ch376->command = CH376_CMD_DIR_INFO_READ;
        ch376->pos_rw_in_cmd_data = 0;
        dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_INFO_READ] waiting for catalog index number\n");
        break;

    case CH376_CMD_DIR_INFO_SAVE:
        ch376->command = CH376_CMD_DIR_INFO_SAVE;
        // mounted?
        if(ch376->root_dir_lock)
        {
            DOS_BOOL is_updated = DOS_FALSE;

            if(ch376->current_file)
            {
                is_updated = system_file_update(&ch376->context, ch376->current_file, &ch376->cmd_data.CMD_DataWrite.WR_FatDirInfo);
            }
            else if(!system_is_root_dir(&ch376->context, ch376->current_dir_lock, ch376->root_dir_lock))
            {
                is_updated = system_directory_update(&ch376->context, ch376->current_dir_lock, &ch376->cmd_data.CMD_DataWrite.WR_FatDirInfo);
            }

            if(is_updated)
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_INFO_SAVE] file updated\n");

                ch376->interface_status = 127;
                ch376->command_status = CH376_INT_SUCCESS;
            }
            else
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_INFO_SAVE] no operation possible (no opened file)\n");

                ch376->interface_status = 0;
                ch376->command_status = CH376_RET_ABORT;
            }
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_FILE_ERASE] no operation possible (device not mounted)\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    default:
        dbg_printf("[WRITE][COMMAND][Unsupported] command &%02x not implemented\n", ch376->command);
        ch376->interface_status = 0;
        ch376->command_status = CH376_RET_ABORT;
        break;
    }

    dbg_printf("<< [WRITE][COMMAND] Write command &%02x status &%02x\n", ch376->command, ch376->command_status);
}

/* /// */

/* /// "CH376 public write data port" */

void ch376_write_data_port(struct ch376 *ch376, DOS_U8 data)
{
    dbg_printf(">> [WRITE][DATA] Write data &%02x status &%02x\n", data, ch376->command_status);

    switch(ch376->command)
    {
    case CH376_CMD_CHECK_EXIST:
        ch376->check_byte = ~data;
        dbg_printf("[WRITE][DATA][CH376_CMD_CHECK_EXIST] got check byte &%02x\n", data);
        break;

    case CH376_CMD_GET_FILE_SIZE:
        // dbg_printf("[WRITE][DATA][CH376_CMD_GET_FILE_SIZE] got &%02x byte\n", data);
        if (data == 0x68)
        {
            DOS_S32 file_size = system_get_file_size(&ch376->context, ch376->current_file);

            ch376->cmd_data.CMD_FileSize[0] = (DOS_U8)((file_size & 0x000000ff) >>  0);
            ch376->cmd_data.CMD_FileSize[1] = (DOS_U8)((file_size & 0x0000ff00) >>  8);
            ch376->cmd_data.CMD_FileSize[2] = (DOS_U8)((file_size & 0x00ff0000) >> 16);
            ch376->cmd_data.CMD_FileSize[3] = (DOS_U8)((file_size & 0xff000000) >> 24);

            ch376->nb_bytes_in_cmd_data = sizeof(ch376->cmd_data.CMD_FileSize);
            ch376->pos_rw_in_cmd_data = 0;

            dbg_printf("[WRITE][DATA][CH376_CMD_GET_FILE_SIZE] done, file size is %d, waiting for data read\n", file_size);

            // Lignes suivantes utiles?
            ch376->interface_status = 127;
            ch376->command_status = CH376_INT_SUCCESS;
        }
        else
        {
            dbg_printf("[WRITE][DATA][CH376_CMD_GET_FILE_SIZE] wrong command byte: looking for &68, got &%02x\n", data);

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    case CH376_CMD_SET_USB_MODE:
        cancel_all_io(ch376);
        switch(data)
        {
        case CH376_ARG_SET_USB_MODE_USB_HOST:
            ch376->usb_mode = CH376_ARG_SET_USB_MODE_USB_HOST;
            dbg_printf("[WRITE][DATA][CH376_SET_USB_MODE] USB host set\n");
            break;
        case CH376_ARG_SET_USB_MODE_SD_HOST:
            ch376->usb_mode = CH376_ARG_SET_USB_MODE_SD_HOST;
            dbg_printf("[WRITE][DATA][CH376_SET_USB_MODE] SD card set\n");
            break;
        default:
            ch376->usb_mode = CH376_ARG_SET_USB_MODE_INVALID;
            dbg_printf("[WRITE][DATA][CH376_SET_USB_MODE_CODE_INVALID] set\n");
            break;
        }
        break;

    case CH376_CMD_SET_FILE_NAME:
        dbg_printf("[WRITE][DATA][CH376_CMD_SET_FILE_NAME] got file name character \"%c\" (&%02x) for position %d\n", data, data, ch376->pos_rw_in_cmd_data);
        // protect buffer overflow
        if(ch376->pos_rw_in_cmd_data < sizeof(ch376->cmd_data.CMD_FileName))
        {
            // store new filename character
            ch376->cmd_data.CMD_FileName[ch376->pos_rw_in_cmd_data++] = data;
            ch376->cmd_data.CMD_FileName[ch376->pos_rw_in_cmd_data] = '\0';
        }
        break;

    case CH376_CMD_BYTE_LOCATE:
        if(ch376->pos_rw_in_cmd_data < sizeof(ch376->cmd_data.CMD_FileSeek))
        {
            dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_LOCATE] got byte #%d with value &%02x\n", ch376->pos_rw_in_cmd_data, data);
            ch376->cmd_data.CMD_FileSeek[ch376->pos_rw_in_cmd_data] = data;

            if(++ch376->pos_rw_in_cmd_data == sizeof(ch376->cmd_data.CMD_FileSeek))
            {
                DOS_S32 file_seek_pos = (ch376->cmd_data.CMD_FileSeek[0] <<  0)
                                      | (ch376->cmd_data.CMD_FileSeek[1] <<  8)
                                      | (ch376->cmd_data.CMD_FileSeek[2] << 16)
                                      | (ch376->cmd_data.CMD_FileSeek[3] << 24);

                file_seek_pos = system_file_seek(&ch376->context, ch376->current_file, file_seek_pos);

                if(file_seek_pos >= 0)
                {
                    dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_LOCATE] all done: seek operated with success (waiting for data read)\n");

                    ch376->cmd_data.CMD_FileSeek[0] = (DOS_U8)((file_seek_pos & 0x000000ff) >>  0);
                    ch376->cmd_data.CMD_FileSeek[1] = (DOS_U8)((file_seek_pos & 0x0000ff00) >>  8);
                    ch376->cmd_data.CMD_FileSeek[2] = (DOS_U8)((file_seek_pos & 0x00ff0000) >> 16);
                    ch376->cmd_data.CMD_FileSeek[3] = (DOS_U8)((file_seek_pos & 0xff000000) >> 24);

                    ch376->nb_bytes_in_cmd_data = sizeof(ch376->cmd_data.CMD_FileSeek);
                    ch376->interface_status = 127;
                    ch376->command_status = CH376_INT_SUCCESS;
                }
                else
                {
                    dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_LOCATE] all done: seek failure\n");

                    ch376->interface_status = 0;
                    ch376->command_status = CH376_RET_ABORT;
                }
            }
        }
        break;

    case CH376_CMD_WR_REQ_DATA:
        if(ch376->nb_bytes_in_cmd_data
        && ch376->pos_rw_in_cmd_data != CMD_DATA_REQ_SIZE)
        {
            DOS_U8 *raw = (DOS_U8 *)&ch376->cmd_data;

            if(ch376->nb_bytes_in_cmd_data != ch376->pos_rw_in_cmd_data)
            {
                raw[ch376->pos_rw_in_cmd_data] = data;

                dbg_printf("[WRITE][DATA][CH376_CMD_WR_REQ_DATA] write \"%c\" (&%02x) to i/o buffer at position &%02x\n", data, data, ch376->pos_rw_in_cmd_data);

                if(++ch376->pos_rw_in_cmd_data == ch376->nb_bytes_in_cmd_data)
                {
                    ch376->interface_status = 0;
                    ch376->command_status = CH376_RET_SUCCESS;
                }
            }
        }
        else
        {
            dbg_printf("[WRITE][DATA][CH376_CMD_WR_REQ_DATA] nothing to write to i/o buffer\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    case CH376_CMD_BYTE_READ:
        if(ch376->pos_rw_in_cmd_data < sizeof(ch376->cmd_data.CMD_FileReadWrite))
        {
            dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_READ] got byte #%d with value &%02x\n", ch376->pos_rw_in_cmd_data, data);
            ch376->cmd_data.CMD_FileReadWrite[ch376->pos_rw_in_cmd_data] = data;

            if(++ch376->pos_rw_in_cmd_data == sizeof(ch376->cmd_data.CMD_FileReadWrite))
            {
                ch376->bytes_to_read_write = (ch376->cmd_data.CMD_FileReadWrite[0] <<  0)
                                           | (ch376->cmd_data.CMD_FileReadWrite[1] <<  8);
                ch376->buffer_read_count = 0; // Init read buffer count (also needed when opendir?)
                file_read_chunk(ch376);
            }
        }
        break;

    case CH376_CMD_BYTE_WRITE:
        if(ch376->pos_rw_in_cmd_data < sizeof(ch376->cmd_data.CMD_FileReadWrite))
        {
            dbg_printf("[WRITE][DATA][CH376_CMD_BYTE_WRITE] got byte #%d with value &%02x\n", ch376->pos_rw_in_cmd_data, data);
            ch376->cmd_data.CMD_FileReadWrite[ch376->pos_rw_in_cmd_data] = data;

            if(++ch376->pos_rw_in_cmd_data == sizeof(ch376->cmd_data.CMD_FileReadWrite))
            {
                ch376->bytes_to_read_write = (ch376->cmd_data.CMD_FileReadWrite[0] <<  0)
                                           | (ch376->cmd_data.CMD_FileReadWrite[1] <<  8);

                if(ch376->bytes_to_read_write > sizeof(ch376->cmd_data.CMD_IOBuffer))
                    ch376->nb_bytes_in_cmd_data = sizeof(ch376->cmd_data.CMD_IOBuffer);
                else
                    ch376->nb_bytes_in_cmd_data = (DOS_U8)ch376->bytes_to_read_write;

                ch376->interface_status = 127;
                ch376->command_status = CH376_INT_SUCCESS;
            }
        }
        break;

    case CH376_CMD_FILE_CLOSE:
        dbg_printf("[WRITE][DATA][CH376_CMD_FILE_CLOSE] update size: %s\n", data == 0 ? "no" : "yes");
        if(ch376->current_file)
        {
            if(data == 1 && ch376->bytes_were_written)
            {
                //system_file_set_size(&ch376->context, ch376->current_file);
                ch376->bytes_were_written = DOS_FALSE;
            }
            system_file_close(&ch376->context, ch376->current_file);
            ch376->current_file = (DOS_FILE)0;

            ch376->interface_status = 127;
            ch376->command_status = CH376_INT_SUCCESS;
        }
        else
        {
            dbg_printf("[WRITE][DATA][CH376_CMD_FILE_CLOSE] failure: not file opened\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;

    case CH376_CMD_WR_OFS_DATA:
        dbg_printf("[WRITE][DATA][CH376_CMD_WR_OFS_DATA] got byte #%d with value &%02x\n", ch376->pos_rw_in_cmd_data, data);
        if(ch376->cmd_data.CMD_DataWrite.WR_Configured == DOS_FALSE)
        {
            if(ch376->pos_rw_in_cmd_data == 0)
            {
                ch376->cmd_data.CMD_DataWrite.WR_Offset = data;
                ch376->pos_rw_in_cmd_data++;
            }
            else if(ch376->pos_rw_in_cmd_data == 1)
            {
                ch376->cmd_data.CMD_DataWrite.WR_Length = data;
                ch376->pos_rw_in_cmd_data++;

                if(ch376->cmd_data.CMD_DataWrite.WR_Offset + ch376->cmd_data.CMD_DataWrite.WR_Length <= 32
                // For safety, we also forbid cluster update which is not emulated
                && (ch376->cmd_data.CMD_DataWrite.WR_Offset + ch376->cmd_data.CMD_DataWrite.WR_Length <= 20
                || (ch376->cmd_data.CMD_DataWrite.WR_Offset > 21 && ch376->cmd_data.CMD_DataWrite.WR_Offset + ch376->cmd_data.CMD_DataWrite.WR_Length <= 26)
                || (ch376->cmd_data.CMD_DataWrite.WR_Offset > 27)))
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_WR_OFS_DATA] waiting for data to update buffer\n");

                    ch376->nb_bytes_in_cmd_data = ch376->cmd_data.CMD_DataWrite.WR_Length + 2;
                    ch376->cmd_data.CMD_DataWrite.WR_Configured = DOS_TRUE;
                }
                else
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_WR_OFS_DATA] invalid offset and or length (unsupported cluster update attempt?)\n");
                    // Make any subsequent access invalid
                    if(ch376->current_file)
                    {
                        system_file_close(&ch376->context, ch376->current_file);
                        ch376->current_file = (DOS_FILE)0;
                    }

                    system_release_directory_lock(&ch376->context, ch376->current_dir_lock);
                    ch376->current_dir_lock = system_clone_directory_lock(&ch376->context, ch376->root_dir_lock);

                    ch376->interface_status = 0;
                    ch376->command_status = CH376_RET_ABORT;
                }
            }
        }
        else
        {
            if(ch376->nb_bytes_in_cmd_data != ch376->pos_rw_in_cmd_data)
            {
                DOS_U8 *raw = (DOS_U8 *)&ch376->cmd_data;
                DOS_U8 pos = ch376->pos_rw_in_cmd_data + ch376->cmd_data.CMD_DataWrite.WR_Offset - 2 /*ofs+len*/;

                raw[pos] = data;

                dbg_printf("[WRITE][DATA][CH376_CMD_WR_OFS_DATA] write \"%c\" (&%02x) to i/o buffer at position &%02x\n", data, data, ch376->pos_rw_in_cmd_data);

                if(++ch376->pos_rw_in_cmd_data == ch376->nb_bytes_in_cmd_data)
                {
                    dbg_printf("[WRITE][COMMAND][CH376_CMD_WR_OFS_DATA] update done\n");
                    ch376->interface_status = 0;
                    ch376->command_status = CH376_RET_SUCCESS;
                }
            }
        }
        break;

    case CH376_CMD_DIR_INFO_READ:
        // Note: catalog index is not implemented; returned is always opened file entry
        dbg_printf("[WRITE][DATA][CH376_CMD_DIR_INFO_READ] catalog index: %d\n", data);
        // mounted?
        if(ch376->root_dir_lock)
        {
            DOS_BOOL is_info_read = DOS_FALSE;

            if(ch376->current_file)
            {
                is_info_read = system_file_info_read(&ch376->context, ch376->current_file, &ch376->cmd_data.CMD_FatDirInfo);
            }
            else if(!system_is_root_dir(&ch376->context, ch376->current_dir_lock, ch376->root_dir_lock))
            {
                is_info_read = system_directory_info_read(&ch376->context, ch376->current_dir_lock, &ch376->cmd_data.CMD_FatDirInfo);
            }

            if(is_info_read)
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_INFO_READ] get directory information done (waiting for data read)\n");
                ch376->nb_bytes_in_cmd_data = sizeof(ch376->cmd_data.CMD_FatDirInfo);
                ch376->interface_status = 127;
                ch376->command_status = CH376_INT_SUCCESS;
            }
            else
            {
                dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_INFO_READ] file not opened\n");
                ch376->interface_status = 0;
                ch376->command_status = CH376_RET_ABORT;
            }
        }
        else
        {
            dbg_printf("[WRITE][COMMAND][CH376_CMD_DIR_INFO_READ] no operation possible (device not mounted)\n");

            ch376->interface_status = 0;
            ch376->command_status = CH376_RET_ABORT;
        }
        break;
    }

    dbg_printf("<< [WRITE][DATA] Write data &%02x status &%02x\n", data, ch376->command_status);
}

/* /// */

/* /// "CH376 public init & clean" */

struct ch376 * ch376_create(void *user_data)
{
    struct ch376 *ch376 = (struct ch376*)system_alloc_mem(sizeof(struct ch376));

    if(ch376)
    {
        if(system_init_context(&ch376->context, user_data))
        {
            ch376->sdcard_drive_path = system_clone_string("ch376_sdcard_drive/");
            ch376->usb_drive_path = system_clone_string("ch376_usb_drive/");
            clear_structure(ch376);
        }
        else
        {
            system_free_mem(ch376);
            ch376 = NULL;
        }
    }
    return ch376;
}

void ch376_destroy(struct ch376 *ch376)
{
    cancel_all_io(ch376);
    system_free_mem(ch376->sdcard_drive_path);
    system_free_mem(ch376->usb_drive_path);
    system_clean_context(&ch376->context);
    system_free_mem(ch376);
}

void ch376_reset(struct ch376 *ch376)
{
    cancel_all_io(ch376);
    clear_structure(ch376);
}

/* /// */

/* /// "CH376 public configuration" */

void ch376_set_sdcard_drive_path(struct ch376 *ch376, const DOS_TEXT *path)
{
    dbg_printf("ch376_set_sdcard_drive_path: %s\n", path);
    cancel_all_io(ch376);
    system_free_mem(ch376->sdcard_drive_path);
    ch376->sdcard_drive_path = system_clone_string(path);
}

void ch376_set_usb_drive_path(struct ch376 *ch376, const DOS_TEXT *path)
{
    dbg_printf("ch376_set_usb_drive_path: %s\n", path);
    cancel_all_io(ch376);
    system_free_mem(ch376->usb_drive_path);
    ch376->usb_drive_path = system_clone_string(path);
}

const DOS_TEXT * ch376_get_sdcard_drive_path(struct ch376 *ch376)
{
    return ch376->sdcard_drive_path;
}

const DOS_TEXT * ch376_get_usb_drive_path(struct ch376 *ch376)
{
    return ch376->usb_drive_path;
}

/* /// */
