/********************************************************************
 *                                                                  *
 * Portable DOS                                                     *
 *                                                                  *
 * Code:                                                            *
 *   J�r�me 'Jede' Debrune                                          *
 *   Philippe 'OffseT' Rimauro                                      *
 *   Christian 'Assinie' Lardi�re                                   *
 *                                                                  *
 ** ch376.h *********************************************************/

#ifndef LOCAL_DOS_H
#define LOCAL_DOS_H

/* /// "Portable types" */

#if defined(__MORPHOS__) || defined (__AMIGA__) || defined (__AROS__)

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef DOS_DOS_H
#include <dos/dos.h>
#endif

typedef UBYTE DOS_U8;
typedef BYTE DOS_S8;
typedef UWORD DOS_U16;
typedef WORD DOS_S16;
typedef ULONG DOS_U32;
typedef LONG DOS_S32;
typedef UQUAD DOS_U64;
typedef QUAD DOS_S64;
typedef BOOL DOS_BOOL;
typedef TEXT DOS_TEXT;
typedef APTR DOS_APTR;
typedef BPTR DOS_FILE;
typedef BPTR DOS_LOCK;
typedef struct _DOS_DIR { struct FileInfoBlock *fib; ULONG ExNextStep; DOS_TEXT pattern[14]; } * DOS_DIR;
typedef struct { struct Library *DOSBase; struct Library *UtilityBase; } DOS_CONTEXT;

#define DOS_TRUE  TRUE
#define DOS_FALSE FALSE

#ifndef UNUSED
#ifdef __GNUC__
#define UNUSED __attribute__((unused))
#else
#define UNUSED
#endif
#endif

#ifndef FIBB_HOLD
#define FIBB_HOLD         7
#endif
#ifndef FIBF_HOLD
#define FIBF_HOLD         (1<<FIBB_HOLD)
#endif

#elif defined(WIN32)

#include <windows.h>

typedef UINT8 DOS_U8;
typedef INT8 DOS_S8;
typedef UINT16 DOS_U16;
typedef INT16 DOS_S16;
typedef UINT32 DOS_U32;
typedef INT32 DOS_S32;
typedef UINT64 DOS_U64;
typedef INT64 DOS_S64;
typedef BOOL DOS_BOOL;
typedef char DOS_TEXT;
typedef void * DOS_APTR;
typedef HANDLE DOS_FILE;
typedef struct _DOS_LOCK { TCHAR path[MAX_PATH]; HANDLE handle; } * DOS_LOCK;
typedef WIN32_FIND_DATA * DOS_DIR;

typedef struct { void *dummy; } DOS_CONTEXT;

#define DOS_TRUE  TRUE
#define DOS_FALSE FALSE

#define UNUSED

#elif defined(__unix__) || defined(__HAIKU__)

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <dirent.h>
#include <limits.h>

typedef uint8_t DOS_U8;
typedef int8_t DOS_S8;
typedef uint16_t DOS_U16;
typedef int16_t DOS_S16;
typedef uint32_t DOS_U32;
typedef int32_t DOS_S32;
typedef uint64_t DOS_U64;
typedef int64_t DOS_S64;
typedef bool DOS_BOOL;
typedef char DOS_TEXT;
typedef void * DOS_APTR;
typedef struct _DOS_FILE { FILE* file; DOS_TEXT path[PATH_MAX]; }* DOS_FILE;
typedef char * DOS_LOCK;
typedef struct _DOS_DIR { DIR *handle; struct dirent *entry; DOS_TEXT pattern[14]; } * DOS_DIR;
typedef struct { void *dummy; } DOS_CONTEXT;

#define DOS_TRUE  true
#define DOS_FALSE false

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

#else
#error "FixMe!"
#endif

// Attributes
#define DIR_ATTR_READ_ONLY 0x01
#define DIR_ATTR_HIDDEN    0x02
#define DIR_ATTR_SYSTEM    0x04
#define DIR_ATTR_VOLUME_ID 0x08
#define DIR_ATTR_DIRECTORY 0x10
#define DIR_ATTR_ARCHIVE   0x20
// Time = (Hour<<11) + (Minute<<5) + (Second>>1)
#define DIR_MAKE_FILE_TIME(h, m, s) (((h)<<11) | ((m)<<5) | ((s)>>1))
// Date = ((Year-1980)<<9) + (Month<<5) + Day
#define DIR_MAKE_FILE_DATE(y, m, d) ((((y)-1980)<<9) | ((m)<<5) | (d))

#define DIR_GET_HOURS(t) (((t)>>11) & 0x1f)
#define DIR_GET_MINS(t)  (((t)>>5) & 0x3f)
#define DIR_GET_SECS(t)  (((t)<<1) & 0x1f)

#define DIR_GET_YEAR(d)  ((((d)>>9) & 0x7f) + 1980)
#define DIR_GET_MONTH(d) (((d)>>5) & 0x0f)
#define DIR_GET_DAY(d)   (((d) & 0x1f))


#pragma pack(1)

// Important: All values in all structures are stored in little-endian (either in 16 or 32 bit)
struct FatDirInfo
{
    DOS_TEXT DIR_Name[11];
    DOS_U8   DIR_Attr;
    DOS_U8   DIR_NTRes;
    DOS_U8   DIR_CrtTimeTenth;
    DOS_U8   DIR_CrtTime[2];
    DOS_U8   DIR_CrtDate[2];
    DOS_U8   DIR_LstAccDate[2];
    DOS_U8   DIR_FstClusHI[2];
    DOS_U8   DIR_WrtTime[2];
    DOS_U8   DIR_WrtDate[2];
    DOS_U8   DIR_FstClusLO[2];
    DOS_U8   DIR_FileSize[4];
};

struct DiskQuery
{
    DOS_U8 DISK_TotalSector[4];
    DOS_U8 DISK_FreeSector[4];
    DOS_U8 DISK_DiskFat;
};

#pragma pack(0)

/* /// */

/* /// "System agnostic DOS functions" */

// Allocate "size" byte of memory
DOS_APTR system_alloc_mem(int size);

// Free memory previously allocated with system_alloc_mem
// It is safe to call it with NULL
void system_free_mem(DOS_APTR ptr);

// Initialize the operating system dependent context
// User data may be used if external data is required for such initialization
DOS_BOOL system_init_context(DOS_CONTEXT *context, UNUSED DOS_APTR user_data);

// Release the operating system dependent context previously initialized by system_init_context
void system_clean_context(DOS_CONTEXT *context);

// Returns true if the dir_lock points to the root_dir
DOS_BOOL system_is_root_dir(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_LOCK root_dir);

// Fill the disk_info structure with information of the disk from which the lock was obtained
DOS_BOOL system_get_disk_info(DOS_CONTEXT *context, DOS_LOCK root_lock, struct DiskQuery *disk_info);

// Try to lock the directory from the provided lock location and return an associated lock
// Return 0 if a directory could not be locked (not a directory or not existing)
DOS_LOCK system_obtain_directory_lock(DOS_CONTEXT *context, const DOS_TEXT *dir_path, DOS_LOCK root_lock);

// Release a lock previouly obtained from system_obtain_directory_lock
// It is safe to call it with a 0
void system_release_directory_lock(DOS_CONTEXT *context, DOS_LOCK dir_lock);

// Clone a directory lock
DOS_LOCK system_clone_directory_lock(DOS_CONTEXT *context, DOS_LOCK dir_lock);

// Create a new directory
DOS_LOCK system_create_directory(DOS_CONTEXT *context, const DOS_TEXT *dir_path, DOS_LOCK root_lock);

// Return a file handle corresponding to the given file located in the provited directory lock
// Return 0 if the file could not be found
DOS_FILE system_file_open_existing(DOS_CONTEXT *context, const DOS_TEXT *file_name, DOS_LOCK root_lock);

// Create a file in the provited directory lock and return its file handle
// Return 0 if the file could not be created
DOS_FILE system_file_open_new(DOS_CONTEXT *context, const DOS_TEXT *file_name, DOS_LOCK root_lock);

// Close a file opened with system_file_open_existing or system_file_open_new
void system_file_close(DOS_CONTEXT *context, DOS_FILE file);

// Seek into a file (absolute position from beginning of the file)
DOS_S32 system_file_seek(DOS_CONTEXT *context, DOS_FILE file, int pos);

// Set the new size of a file depending on the current seek position
DOS_S32 system_file_set_size(DOS_CONTEXT *context, DOS_FILE file);

// Read a part of a file
DOS_S32 system_file_read(DOS_CONTEXT *context, DOS_FILE file, DOS_APTR buffer, DOS_S32 size);

// Write some data into a file
DOS_S32 system_file_write(DOS_CONTEXT *context, DOS_FILE file, DOS_APTR buffer, DOS_S32 size);

// Delete a file (file becomes invalid and shall not be used anymore after this call)
DOS_BOOL system_file_delete(DOS_CONTEXT *context, DOS_FILE file);

// Delete a directory (dir_lock becomes invalid and shall not be used anymore after this call)
DOS_BOOL system_directory_delete(DOS_CONTEXT *context, DOS_LOCK dir_lock);

// Read informations about a file and feed dir_info contents accordingly
DOS_BOOL system_file_info_read(DOS_CONTEXT *context, DOS_FILE file, struct FatDirInfo *dir_info);

// Read informations about a directory and feed dir_info contents accordingly
DOS_BOOL system_directory_info_read(DOS_CONTEXT *context, DOS_LOCK dir_lock, struct FatDirInfo *dir_info);

// Update a file name and attributes according to dir_info contents
DOS_BOOL system_file_update(DOS_CONTEXT *context, DOS_FILE file, struct FatDirInfo *dir_info);

// Update a directory name and attributes according to dir_info contents
DOS_BOOL system_directory_update(DOS_CONTEXT *context, DOS_LOCK dir_lock, struct FatDirInfo *dir_info);

// Start examining a directory
// Return information about the directory itself
DOS_DIR system_start_examine_directory(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_TEXT *pattern);

// Get the next entry from a directory on which a examine session was started using system_start_examine_directory
DOS_BOOL system_go_examine_directory(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_DIR dir, struct FatDirInfo *dir_info);

// Finish a directory examine session (release all related resources)
void system_finish_examine_directory(DOS_CONTEXT *context, DOS_DIR dir);

// Get the size of the current file
DOS_S32 system_get_file_size(DOS_CONTEXT *context, DOS_FILE file);

/* /// */

/* /// "Helper functions" */

DOS_TEXT * system_clone_string(const DOS_TEXT *string);
const DOS_TEXT * system_trim_file_name(const DOS_TEXT *file_name, DOS_TEXT *trimmed_file_name);

/* /// */

#endif /* LOCAL_DOS_H */
