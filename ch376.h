/********************************************************************
 *                                                                  *
 * Very minimal CH376 emulation                                     *
 *                                                                  *
 * Code:                                                            *
 *   J�r�me 'Jede' Debrune                                          *
 *   Philippe 'OffseT' Rimauro                                      *
 *   Christian 'Assinie' Lardi�re                                   *
 *                                                                  *
 ** ch376.h *********************************************************/

#ifndef LOCAL_CH376_H
#define LOCAL_CH376_H

/* /// "Includes" */

#include "dos.h"

/* /// */

/* /// "CH376 public I/O API" */

struct ch376;

// Initialization
struct ch376 * ch376_create(DOS_APTR user_data);
void ch376_destroy(struct ch376 *ch376);
void ch376_reset(struct ch376 *ch376);

// Configuration
void ch376_set_sdcard_drive_path(struct ch376 *ch376, const DOS_TEXT *path);
void ch376_set_usb_drive_path(struct ch376 *ch376, const DOS_TEXT *path);
const DOS_TEXT * ch376_get_sdcard_drive_path(struct ch376 *ch376);
const DOS_TEXT * ch376_get_usb_drive_path(struct ch376 *ch376);

// Runtime
void ch376_write_command_port(struct ch376 *ch376, DOS_U8 command);
void ch376_write_data_port(struct ch376 *ch376, DOS_U8 data);
DOS_U8 ch376_read_command_port(struct ch376 *ch376);
DOS_U8 ch376_read_data_port(struct ch376 *ch376);

/* /// */

#endif
