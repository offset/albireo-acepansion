/*
** Graphical User Interface using the Be API
** for Albireo ACEpansion
*/


#include <Box.h>
#include <GridView.h>
#include <LayoutBuilder.h>
#include <StringView.h>
#include <View.h>

#include "intuition/classusr.h"
#include "exec/types.h"
#include "sys/haiku/PopASL.h"

#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */

#if 0
    struct Hook setDrivePathHook;

struct Hook {
	PluginData* plugin;
	int drive;
};
#endif

static void setDrivePathHandler(void*, const char*);
static PopASL* MakeDrivePathSelector(CONST_STRPTR drivePath, CONST_STRPTR help);

class Container: public BBox
{
public:
	Container(ACEpansionPlugin* plugin)
		: BBox("albireo settings")
		, myPlugin(plugin)
	{
		SetLabel(GetString(MSG_DRIVES));

		BGridView* grid = new BGridView();
		AddChild(grid);

		BLayoutBuilder::Grid<>(grid)
			.SetInsets(B_USE_DEFAULT_SPACING)
			.Add(new BStringView("sd", GetString(MSG_SDCARD_DRIVE)), 0, 0)
			.Add(TXT_SDCardDrive = MakeDrivePathSelector(NULL, GetString(MSG_SDCARD_DRIVE_HELP)), 1, 0)
			.Add(new BStringView("usb", GetString(MSG_USB_DRIVE)), 0, 1)
			.Add(TXT_USBDrive = MakeDrivePathSelector(NULL, GetString(MSG_USB_DRIVE_HELP)), 1, 1)
		.End();

		TXT_SDCardDrive->SetMessage(new BMessage('SDCD'));
		TXT_USBDrive->SetMessage(new BMessage('USBD'));
	}

	void AttachedToWindow()
	{
		BBox::AttachedToWindow();
		TXT_SDCardDrive->SetTarget(this);
		TXT_USBDrive->SetTarget(this);
	}

	void MessageReceived(BMessage* message)
	{
		switch(message->what)
		{
			case 'SDCD':
                Plugin_SetSDCardDrivePath(myPlugin, message->FindString("Name"));
				break;
			case 'USBD':
                Plugin_SetUSBDrivePath(myPlugin, message->FindString("Name"));
				break;
			default:
				BBox::MessageReceived(message);
				break;
		}
	}

	void SetSDCardDrivePath(const char* path)
	{
		TXT_SDCardDrive->TextControl()->SetText(path);
	}

	void SetUSBDrivePath(const char* path)
	{
		TXT_USBDrive->TextControl()->SetText(path);
	}
private:
    PopASL *TXT_SDCardDrive;
    PopASL *TXT_USBDrive;

	struct ACEpansionPlugin* myPlugin;
};

/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
	return TRUE;
}


/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
}

Object* GUI_Create(struct ACEpansionPlugin *myPlugin)
{
	BBox* mainBox = new Container(myPlugin);
	return (Object*)mainBox;
}

/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
	BView* view = (BView*)gui;
	BWindow* window = view->Window();

	window->Lock();
	view->RemoveSelf();
	window->Unlock();
	delete view;
}

/*
** Functions called from the plugin
*/
VOID GUI_SetSDCardDrivePath(Object *gui, CONST_STRPTR path)
{
	Container* container = (Container*)gui;
	container->SetSDCardDrivePath(path);
}

VOID GUI_SetUSBDrivePath(Object *gui, CONST_STRPTR path)
{
	Container* container = (Container*)gui;
	container->SetUSBDrivePath(path);
}

static PopASL* MakeDrivePathSelector(CONST_STRPTR drivePath, CONST_STRPTR help)
{
    PopASL* view = new PopASL(0, "", drivePath, GetString(MSG_ASL_REQUEST), "", help,
		B_OPEN_PANEL, true, B_DIRECTORY_NODE);
	return view;
}
