# Makefile for ACEpansion.

# Name of the ACEpansion
ACEPANSION = albireo

# Location where the ACE plugin SDK is installed
ACESDK=/usr/include/

# Path to SimpleCat executable
SIMPLECAT=SimpleCat

# GCC version to use
CC = gcc

CFLAGS += -s -Ofast -noixemul -nostdlib -fomit-frame-pointer
CFLAGS += -D__NOLIBBASE__
CFLAGS += -Wall -Wextra -Wpointer-arith -Wno-pointer-sign
CFLAGS += -I$(ACESDK) -I.

LDFLAGS = -nostartfiles -noixemul

STRIP = strip --strip-unneeded --remove-section .comment
OUTPUT = Release/Plugins/$(ACEPANSION).acepansion

# Always keep lib_dummy.o in first position!
OBJS = o/lib_dummy.o o/acepansion.o o/interface_mui.o o/ch376.o o/dos.o

.PHONY: all clean

all: $(OUTPUT)
	@ls -l $<
	-FlushLib $(notdir $(OUTPUT))

clean:
	-rm -rf $(OBJS) o/$(ACEPANSION).db $(OUTPUT) generated/locale_strings.h Release/Catalogs/*/$(ACEPANSION).acepansion.catalog

o/$(ACEPANSION).db: $(OBJS)
	@echo "Linking $@..."
	@$(CC) $(LDFLAGS) $(OBJS) -o o/$(ACEPANSION).db -ldebug

$(OUTPUT): o/$(ACEPANSION).db
	@echo "Stripping $<..."
	@$(STRIP) -o $(OUTPUT) o/$(ACEPANSION).db

o/acepansion.o: acepansion.c acepansion.h interface.h generated/locale_strings.h ch376.h
	@echo "Compiling $@..."
	@$(CC) $(CFLAGS) -c -o $@ $<

o/interface_mui.o: interface_mui.c interface.h acepansion.h generated/locale_strings.h
	@echo "Compiling $@..."
	@$(CC) $(CFLAGS) -c -o $@ $<

o/ch376.o: ch376.c ch376.h dos.h
	@echo "Compiling $@..."
	@$(CC) $(CFLAGS) -c -o $@ $<

o/dos.o: dos.c dos.h dos-amiga.c dos-unix.c dos-win32.c
	@echo "Compiling $@..."
	@$(CC) $(CFLAGS) -c -o $@ $<

o/lib_dummy.o: $(ACESDK)/acepansion/lib_dummy.c acepansion.h
	@echo "Compiling $@..."
	@$(CC) $(CFLAGS) -c -o $@ $<

generated/locale_strings.h: catalogs.cs
	@echo "Generating catalogs..."
	@$(SIMPLECAT) catalogs.cs QUIET

