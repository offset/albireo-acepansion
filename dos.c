/********************************************************************
 *                                                                  *
 * Portable DOS (29.12.2022)                                        *
 *                                                                  *
 * Code:                                                            *
 *   J�r�me 'Jede' Debrune                                          *
 *   Philippe 'OffseT' Rimauro                                      *
 *   Christian 'Assinie' Lardi�re                                   *
 *                                                                  *
 ** ch376.c *********************************************************/

//#define DEBUG_DOS

/* /// "Portable includes" */

#if defined(__amigaos__)

#ifndef __NOLIBBASE__
#define __NOLIBBASE__
#endif

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <dos/dos.h>
#include <utility/utility.h>
#include <string.h>

extern struct Library *SysBase;

#ifdef DEBUG_DOS
#include <clib/debug_protos.h>
#define dbg_printf kprintf
#else
#define dbg_printf(...)
#endif

#elif defined(_WIN32)

#include <windows.h>
#include <stdio.h>
#include <strsafe.h>
#include <sys/stat.h>
//@iss #include <shlwapi.h>

#ifdef DEBUG_DOS
static void dbg_printf(LPCTSTR str, ...)
{
        TCHAR buffer[256];

        va_list args;
        va_start(args, str);
        _vsnprintf_s(buffer, sizeof(buffer) - 1, sizeof(buffer) - 1, str, args);
        OutputDebugString(buffer);
        va_end(args);
}
#else
#define dbg_printf(...)
#endif

#elif defined(__unix__) || defined(__APPLE__) || defined(__HAIKU__)

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <sys/statvfs.h>
#include <sys/stat.h>

#ifdef DEBUG_DOS
#define dbg_printf(...) fprintf(stderr, __VA_ARGS__)
#else
#define dbg_printf(...)
#endif

#else
#error "FixMe!"
#endif

#include "dos.h"

/* /// */

/* /// "Private helper functions" */

// Check file_name against pattern
// file_name and pattern must be normalized before calling the function
static DOS_BOOL pattern_match(const DOS_TEXT *pattern, const DOS_TEXT *file_name)
{
    DOS_BOOL found = DOS_TRUE;
    int i;

    // Note: %s should not be used here, these strings are not C-strings!
    dbg_printf("pattern_match('%s', '%s') => ", pattern, file_name);

    for(i=0; i<11; i++)
    {
        if(pattern[i] != '?')
        {
            if(pattern[i] != file_name[i])
            {
                found = DOS_FALSE;
                break;
            }
        }
        i++;
    }

    dbg_printf("i=%d, found=%d\n", i, found);

    return found;
}

// Normalize a char (capitalized and ASCII)
// Return -1 if not possible
static int normalize_char(DOS_TEXT c)
{
    switch(c)
    {
        case ' ':
        case '!': case '#': case '$':
        case '%': case '&': case '\'':
        case '(': case ')': case '-':
        case '@': case '^': case '_':
        case '`': case '{': case '}':
        case '~': case '+':
        case '0': case '1': case '2':
        case '3': case '4': case '5':
        case '6': case '7': case '8':
        case '9':
            return c;

        // Allowed here to also alow pattern matching
        case '*':
            return c;

        default:
            if((c >='A' && c <='Z') || (c >= 128 && c <= 228) || c >= 230)
                return c;

            if(c >='a' && c <='z')
                return c - ('a' - 'A');

            return -1;
    }
}

// Normalized a dos filename in 8.3 format
// Return NULL if not possible
static const DOS_TEXT * normalize_file_name(const DOS_TEXT *file_name, DOS_TEXT *normalized_file_name)
{
    int i = 0;
    int j = 0;
    int c;

    dbg_printf("normalize_file_name: '%s'\n", file_name);

    while(file_name[i] != '\0' && file_name[i] != '.' && j < 8)
    {
        c = normalize_char(file_name[i++]);

        if(c == -1)
            return NULL;
        else
            normalized_file_name[j++] = (DOS_TEXT)c;
    }

    if(file_name[i] == '\0')
    {
        for(; j<8; j++)
        {
            normalized_file_name[j] = ' ';
        }
    }
    else if(file_name[i] == '.')
    {
        i++;
        for(; j<8; j++)
        {
            normalized_file_name[j] = ' ';
        }
    }
    else
    {
        dbg_printf("normalize_file_name impossible: '%s' (%d=%c,%d=%c)\n", file_name, i, file_name[i], j, normalized_file_name[j]);
        return NULL;
    }

    while(file_name[i] != '\0' && file_name[i] != '.' && j < 11)
    {
        c = normalize_char(file_name[i++]);

        if(c == -1)
            return NULL;
        else
            normalized_file_name[j++] = (DOS_TEXT)c;
    }

    if(file_name[i] != '\0')
    {
        dbg_printf("normalize_file_name impossible: '%s' (%d=%c,%d=%c)\n", file_name, i, file_name[i], j, normalized_file_name[j]);
        return NULL;
    }

    dbg_printf("normalize_file_name done: '%s' (%d=&%02x,%d=&%02x)\n", file_name, i, file_name[i], j, normalized_file_name[j]);

    for(; j<11; j++)
    {
        normalized_file_name[j] = ' ';
    }

    normalized_file_name[j] = '\0';

    return normalized_file_name;
}

// Normalize file pattern (use normalize_file_name())
static const DOS_TEXT * normalize_pattern(const DOS_TEXT *pattern, DOS_TEXT *normalized_pattern)
{
    int i = -1;

    dbg_printf("normalize_pattern: '%s'\n", normalized_pattern);

    if(pattern[0] == '*' && pattern[1] == '\0')
    {
        // replace "*" by "*.*"
        dbg_printf("normalize_pattern: * -> *.*\n");
        normalize_file_name("*.*", normalized_pattern);
    }
    else if(!normalize_file_name(pattern, normalized_pattern))
    {
        dbg_printf("normalize_pattern: error\n");
        normalized_pattern[0] = '\0'; // if used, pattern_match() will always return FALSE
        return NULL;
    }

    while(i<11)
    {
        i++;
        if(normalized_pattern[i] == '*')
        {
            // Replace '*' by '?' sequence for pattern_match()
            while(i < 11)
                normalized_pattern[i++] = '?';
        }
    }

    // Note: %s should not be used here, this is not a C-string!
    dbg_printf("normalize_pattern: '%s' (i=%d)\n", normalized_pattern, i);

    return normalized_pattern;
}

/* /// */

/* /// "Public helper functions" */

DOS_TEXT * system_clone_string(const DOS_TEXT *string)
{
    int l;
    DOS_TEXT *s;

    for(l=0; string[l]!='\0'; l++);

    s = (DOS_TEXT*)system_alloc_mem(l + 1);

    for(l=0; string[l]!='\0'; l++)
        s[l] = string[l];

    s[l] = '\0';

    return s;
}

// Convert normalized 8.3 filename into regular filename
// (remove trailing spaces in name and extension, and clear extension trailing '.' if any)
const DOS_TEXT * system_trim_file_name(const DOS_TEXT *file_name, DOS_TEXT *trimmed_file_name)
{
    int i = 0;
    int j = 0;

    while(file_name[i] != ' ' && file_name[i] != '\0')
        trimmed_file_name[j++] = file_name[i++];

    while(file_name[i] == ' ')
        i++;

    while(file_name[i] != ' ' && file_name[i] != '\0')
        trimmed_file_name[j++] = file_name[i++];

    if(j > 0 && trimmed_file_name[j-1] == '.')
        j--;

    trimmed_file_name[j] = '\0';

    dbg_printf("system_trim_file_name from \"%s\" to \"%s\"\n", file_name, trimmed_file_name);

    return trimmed_file_name;
}

/* /// */

/* /// "Portable implementation" */

#if defined(__amigaos__)

#include "dos-amiga.c"

#elif defined(_WIN32)

#include "dos-win32.c"

#elif defined(__unix__) || defined(__APPLE__) || defined(__HAIKU__)

#include "dos-unix.c"

#else
#error "FixMe!"
#endif

/* /// */
