/********************************************************************
 *                                                                  *
 * Portable DOS (29.12.2022)                                        *
 *                                                                  *
 * Code:                                                            *
 *   Jérôme 'Jede' Debrune                                          *
 *   Philippe 'OffseT' Rimauro                                      *
 *   Christian 'Assinie' Lardière                                   *
 *                                                                  *
 ** ch376.c *********************************************************/

#include <errno.h>
#include <libgen.h>
#include <time.h>


/* /// "POSIX system functions" */

/* Private */








static bool priv_feed_dir_info(DOS_CONTEXT *context, struct stat *fib, struct FatDirInfo *dir_info)
{

        dir_info->DIR_Attr = 0;

        if (S_ISDIR(fib->st_mode))
            dir_info->DIR_Attr |= DIR_ATTR_DIRECTORY;
        if ((fib->st_mode & S_IWUSR) == 0)
            dir_info->DIR_Attr |= DIR_ATTR_READ_ONLY;

        // FAT permissions don't match well with UNIX ones, do an arbitrary mapping to group permissions
        if ((fib->st_mode & S_IRGRP) == 0)
            dir_info->DIR_Attr |= DIR_ATTR_HIDDEN;
        if ((fib->st_mode & S_IWGRP) == 0)
            dir_info->DIR_Attr |= DIR_ATTR_ARCHIVE;
        if ((fib->st_mode & S_IXGRP) != 0)
            dir_info->DIR_Attr |= DIR_ATTR_SYSTEM;
        dbg_printf("priv_feed_dir_info defined file attributes: %d\n", dir_info->DIR_Attr);

        dir_info->DIR_NTRes = 0;

        struct tm* when = localtime(&fib->st_crtim.tv_sec);
        uint16_t time = DIR_MAKE_FILE_TIME(when->tm_hour, when->tm_min, when->tm_sec);
        uint16_t date = DIR_MAKE_FILE_DATE(when->tm_year, when->tm_mon, when->tm_mday);

        dir_info->DIR_CrtTimeTenth = fib->st_ctim.tv_nsec / 100000000;
        dir_info->DIR_CrtTime[0] = (time & 0x00ff) >> 0;
        dir_info->DIR_CrtTime[1] = (time & 0xff00) >> 8;
        dir_info->DIR_CrtDate[0] = (date & 0x00ff) >> 0;
        dir_info->DIR_CrtDate[1] = (date & 0xff00) >> 8;

        when = localtime(&fib->st_atim.tv_sec);
        date = DIR_MAKE_FILE_DATE(when->tm_year, when->tm_mon, when->tm_mday);

        dir_info->DIR_LstAccDate[0] = (date & 0x00ff) >> 0;
        dir_info->DIR_LstAccDate[1] = (date & 0xff00) >> 8;
        dir_info->DIR_FstClusHI[0] = 0;
        dir_info->DIR_FstClusHI[1] = 0;

        when = localtime(&fib->st_mtim.tv_sec);
        time = DIR_MAKE_FILE_TIME(when->tm_hour, when->tm_min, when->tm_sec);
        date = DIR_MAKE_FILE_DATE(when->tm_year, when->tm_mon, when->tm_mday);

        dir_info->DIR_WrtTime[0] = (time & 0x00ff) >> 0;
        dir_info->DIR_WrtTime[1] = (time & 0xff00) >> 8;
        dir_info->DIR_WrtDate[0] = (date & 0x00ff) >> 0;
        dir_info->DIR_WrtDate[1] = (date & 0xff00) >> 8;
        dir_info->DIR_FstClusLO[0] = 0;
        dir_info->DIR_FstClusLO[1] = 0;
        dir_info->DIR_FileSize[0] = (fib->st_size & 0x000000ff) >>  0;
        dir_info->DIR_FileSize[1] = (fib->st_size & 0x0000ff00) >>  8;
        dir_info->DIR_FileSize[2] = (fib->st_size & 0x00ff0000) >> 16;
        dir_info->DIR_FileSize[3] = (fib->st_size & 0xff000000) >> 24;

        return true;

}

static bool priv_entry_update(DOS_CONTEXT *context, const char* entry_full_path, struct FatDirInfo *dir_info)
{
    dbg_printf("priv_entry_update: %s\n", entry_full_path);

    {
        char new_name[16];
        char fixed_new_name[13]; // Max = 8 + '.' + 3 + '\0'
        int i,j;


        for(i=0,j=0; i<8; i++) new_name[j++] = dir_info->DIR_Name[i];
        new_name[j++] = '.';
        for(;i<11; i++) new_name[j++] = dir_info->DIR_Name[i];
        new_name[j++] = '\0';

        system_trim_file_name(new_name, fixed_new_name);

        const char* last_slash = strrchr(entry_full_path, '/');
        char* new_full_path = (char*)calloc(last_slash - entry_full_path + 2 + strlen(fixed_new_name), 1);
        strncpy(new_full_path, entry_full_path, last_slash - entry_full_path + 1);
        strcat(new_full_path, fixed_new_name);

        // rename only if the new name is different from the old...
        if ((strcmp(new_full_path, entry_full_path) != 0) && (rename(entry_full_path, new_full_path) != 0)) {
            free(new_full_path);
            return false;
        }


        mode_t newmode = S_IRUSR; // Always at least readable by user

        if (!(dir_info->DIR_Attr & DIR_ATTR_READ_ONLY))
            newmode |= S_IWUSR;
        if (!(dir_info->DIR_Attr & DIR_ATTR_HIDDEN))
            newmode |= S_IRGRP;
        if (!(dir_info->DIR_Attr & DIR_ATTR_ARCHIVE))
            newmode |= S_IWGRP;
        if (dir_info->DIR_Attr & DIR_ATTR_SYSTEM)
            newmode |= S_IXGRP;
        if (chmod(new_full_path, newmode) != 0) {
            free(new_full_path);
            return false;
        }

        uint16_t d = dir_info->DIR_WrtDate[0] | (dir_info->DIR_WrtDate[1] << 8);
        uint16_t t = dir_info->DIR_WrtTime[0] | (dir_info->DIR_WrtTime[1] << 8);
        struct tm modtime = { 0 };
        modtime.tm_mday = DIR_GET_DAY(d);
        modtime.tm_mon = DIR_GET_MONTH(d);
        modtime.tm_year = DIR_GET_YEAR(d);
        modtime.tm_hour = DIR_GET_HOURS(t);
        modtime.tm_min = DIR_GET_MINS(t);
        modtime.tm_sec = DIR_GET_SECS(t);

        d = dir_info->DIR_LstAccDate[0] | (dir_info->DIR_LstAccDate[1] << 8);
        struct tm acctime = { 0 };
        acctime.tm_mday = DIR_GET_DAY(d);
        acctime.tm_mon = DIR_GET_MONTH(d);
        acctime.tm_year = DIR_GET_YEAR(d);

        struct timeval times[2] = { 0 };
        times[0].tv_sec = mktime(&modtime);
        times[1].tv_sec = mktime(&acctime);
        if (utimes(new_full_path, times) != 0) {
            free(new_full_path);
            return false;
        }

        free(new_full_path);
    }

    return true;
}

/* Public */

DOS_APTR system_alloc_mem(int size)
{
    return malloc(size);
}

void system_free_mem(DOS_APTR ptr)
{
    if (ptr)
        free(ptr);
}

DOS_BOOL system_init_context(DOS_CONTEXT *context, UNUSED DOS_APTR user_data)
{
  /* Nothing to do */
    return DOS_TRUE;
}

void system_clean_context(DOS_CONTEXT *context)
{
  /* Nothing to do */
}

DOS_BOOL system_is_root_dir(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_LOCK root_dir)
{
    return strcmp(dir_lock, root_dir) == 0;
}

DOS_BOOL system_get_disk_info(DOS_CONTEXT *context, DOS_LOCK root_lock, struct DiskQuery *disk_info)
{
    DOS_BOOL got_info = DOS_FALSE;

    if(disk_info)
    {
        struct statvfs stats;

        if(statvfs(root_lock, &stats) == 0)
        {
            int64_t total_sector = (stats.f_blocks * stats.f_bsize) / 512;
            //int64_t free_sector = (stats.f_bfree * stats.f_bsize) / 512;
            int64_t free_sector = (stats.f_bavail * stats.f_bsize) / 512;

            dbg_printf("\n*** f_frsize=%ld, f_bsize=%ld\n", stats.f_frsize, stats.f_bsize);
            dbg_printf(  "*** f_blocks=%ld, f_bsize=%ld\n", stats.f_blocks, stats.f_bsize);
            dbg_printf(  "*** f_bfree =%ld, f_bsize=%ld\n", stats.f_bfree , stats.f_bsize);
            dbg_printf(  "*** f_bavail=%ld, f_bsize=%ld\n", stats.f_bavail, stats.f_bsize);

            disk_info->DISK_TotalSector[0] = (total_sector & 0x000000ff) >>  0;
            disk_info->DISK_TotalSector[1] = (total_sector & 0x0000ff00) >>  8;
            disk_info->DISK_TotalSector[2] = (total_sector & 0x00ff0000) >> 16;
            disk_info->DISK_TotalSector[3] = (total_sector & 0xff000000) >> 24;

            disk_info->DISK_FreeSector[0] = (free_sector & 0x000000ff) >>  0;
            disk_info->DISK_FreeSector[1] = (free_sector & 0x0000ff00) >>  8;
            disk_info->DISK_FreeSector[2] = (free_sector & 0x00ff0000) >> 16;
            disk_info->DISK_FreeSector[3] = (free_sector & 0xff000000) >> 24;

            disk_info->DISK_DiskFat = 0x0c; // FAT32?

            got_info = DOS_TRUE;
        }
    }

    return got_info;
}

DOS_BOOL system_file_update(DOS_CONTEXT *context, DOS_FILE file, struct FatDirInfo *dir_info)
{
    bool is_done = false;

    dbg_printf("system_file_update: %p\n", file);

    if(file)
    {
        is_done = priv_entry_update(context, file->path, dir_info);
    }
           
    return is_done;
}

DOS_BOOL system_directory_update(DOS_CONTEXT *context, DOS_LOCK dir_lock, struct FatDirInfo *dir_info)
{
    bool is_done = false;

    dbg_printf("system_file_update: %p\n", dir_lock);

    if(dir_lock)
    {
        // In UNIX implementation, dir_lock is just the directory path string
        is_done = priv_entry_update(context, dir_lock, dir_info);
    }

    return is_done;
}

DOS_LOCK system_obtain_directory_lock(DOS_CONTEXT *context, const DOS_TEXT *dir_path, DOS_LOCK root_lock)
{
    DOS_LOCK lock = NULL;
    char *old_dir = NULL;
    struct stat path_stat;

    dbg_printf("system_obtain_directory_lock: %s\n", dir_path);

    if(root_lock)
    {
        if((old_dir = getcwd(NULL, 0)) != NULL)
            chdir(root_lock);
    }

    stat(dir_path, &path_stat);
    if(S_ISDIR(path_stat.st_mode))
    {
      lock = realpath(dir_path, NULL);
    }

    if(old_dir)
    {
        if(old_dir != NULL)
        {
            chdir(old_dir);
            system_free_mem(old_dir);
        }
    }

    return lock;
}

void system_release_directory_lock(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    if(dir_lock != (DOS_LOCK)0)
    {
        system_free_mem(dir_lock);
    }
}

DOS_LOCK system_clone_directory_lock(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    return strdup(dir_lock);
}

DOS_LOCK system_create_directory(DOS_CONTEXT *context, const DOS_TEXT *dir_path, DOS_LOCK root_lock)
{
    DOS_LOCK lock = NULL;
    char *old_dir = NULL;

    if(root_lock)
    {
        if((old_dir = getcwd(NULL, 0)) != NULL)
            chdir(root_lock);
    }

    dbg_printf("system_create_directory: %s\n", dir_path);

    if (!mkdir(dir_path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH))
        lock = realpath(dir_path, NULL);

    if(old_dir)
    {
        chdir(old_dir);
        system_free_mem(old_dir);
    }

    return lock;
}

static DOS_FILE file_open(const char *file_name,  DOS_LOCK root_lock, const char *mode)
{
    DOS_FILE fp = new _DOS_FILE;
    char *old_dir = NULL;

    if(root_lock)
    {
        if((old_dir = getcwd(NULL, 0)) != NULL)
            chdir(root_lock);
    }

    fp->file = fopen(file_name, mode);
    realpath(file_name, fp->path);

    if(old_dir)
    {
        chdir(old_dir);
        system_free_mem(old_dir);
    }

    if (fp->file == NULL) {
        delete fp;
        return NULL;
    }
    return fp;
}

DOS_FILE system_file_open_existing(DOS_CONTEXT *context, const DOS_TEXT *file_name, DOS_LOCK root_lock)
{
    return file_open(file_name, root_lock, "rb+");
}

DOS_FILE system_file_open_new(DOS_CONTEXT *context, const DOS_TEXT *file_name, DOS_LOCK root_lock)
{
    return file_open(file_name, root_lock, "wb+");
}

void system_file_close(DOS_CONTEXT *context, DOS_FILE file)
{
    if (file) fclose(file->file);
}

DOS_S32 system_file_seek(DOS_CONTEXT *context, DOS_FILE file, int pos)
{
    dbg_printf("system_file_seek trying to seek to position %d\n", pos);

    if(file)
        if (!fseek(file->file, pos, SEEK_SET))
            return ftell(file->file);
//    else
        return -1;
}

DOS_S32 system_file_read(DOS_CONTEXT *context, DOS_FILE file, DOS_APTR buffer, DOS_S32 size)
{
    dbg_printf("system_file_read trying to read %d bytes\n", size);

    if(file)
        return fread(buffer, 1, size, file->file);
    else
        return -1;
}

DOS_S32 system_file_write(DOS_CONTEXT *context, DOS_FILE file, DOS_APTR buffer, DOS_S32 size)
{
    dbg_printf("system_file_write trying to write %d bytes\n", size);

    if(file)
        return fwrite(buffer, 1, size, file->file);
    else
        return -1;
}

DOS_BOOL system_file_delete(DOS_CONTEXT *context, DOS_FILE file)
{
    int err;

    err = remove(file->path);

    dbg_printf("system_file_delete: %p\n", file);

    return (err == 0);
}

DOS_BOOL system_directory_delete(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    dbg_printf("system_directory_delete: %p\n", dir_lock);

    if (dir_lock)
    {
        if(rmdir(dir_lock) == 0)
        {
            system_release_directory_lock(context, dir_lock);
            return DOS_TRUE;
        }

        return DOS_FALSE;
    }
    else
        return DOS_FALSE;
}

DOS_BOOL system_file_info_read(DOS_CONTEXT *context, DOS_FILE file, struct FatDirInfo *dir_info)
{
    bool is_done = false;

    dbg_printf("system_file_read_info: %p\n", file);

    if(file)
    {
        struct stat fib;
        if (fstat(fileno(file->file), &fib) == 0)
        {
            if(normalize_file_name(basename(file->path), dir_info->DIR_Name)) {
                is_done = priv_feed_dir_info(context, &fib, dir_info);
            }
        }

    }

    return is_done;
}

DOS_BOOL system_directory_info_read(DOS_CONTEXT *context, DOS_LOCK dir_lock, struct FatDirInfo *dir_info)
{
    bool is_done = false;

    dbg_printf("system_directory_read_info: %p\n", dir_lock);

    if(dir_lock)
    {
        struct stat fib;

        if (stat(dir_lock, &fib) == 0)
        {
            if(normalize_file_name(basename(dir_lock), dir_info->DIR_Name))
            {
                is_done = priv_feed_dir_info(context, &fib, dir_info);
            }
        }

    }

    return is_done;
}

DOS_DIR system_start_examine_directory(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_TEXT* pattern)
{
    DOS_DIR fib = (DOS_DIR)system_alloc_mem(sizeof(struct _DOS_DIR));

    if(fib)
    {
        fib->handle = opendir(dir_lock);
        fib->entry = NULL;
        normalize_pattern(pattern, fib->pattern);
    }

    return fib;
}

DOS_BOOL system_go_examine_directory(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_DIR fib, struct FatDirInfo *dir_info)
{
    DOS_BOOL is_done = DOS_FALSE;
    struct stat file_stat;
    struct tm *timeinfo;
    DOS_U16 dos_cdate, dos_ctime;
    DOS_U16 dos_adate;
    DOS_U16 dos_mdate, dos_mtime;
    char *old_dir = NULL;

    if(dir_lock)
    {
        if((old_dir = getcwd(NULL, 0)) != NULL)
            chdir(dir_lock);
    }

    if(fib) while(!is_done && (fib->entry = readdir(fib->handle)))
    {
        if(normalize_file_name(fib->entry->d_name, dir_info->DIR_Name) && pattern_match(fib->pattern, dir_info->DIR_Name))
        {
            stat(fib->entry->d_name, &file_stat);

            dir_info->DIR_Attr = 0;

            if(S_ISDIR(file_stat.st_mode))
                dir_info->DIR_Attr |= DIR_ATTR_DIRECTORY;
            if((file_stat.st_mode & S_IWUSR) == 0) {
                dir_info->DIR_Attr |= DIR_ATTR_READ_ONLY;
            dbg_printf("--- READ ONLY ---");
            }

            dbg_printf("system_go_examine_directory defined file attributes: %04o -> %02x\n", file_stat.st_mode, dir_info->DIR_Attr);
            if (file_stat.st_ctim.tv_sec == 0)
            {
                dos_cdate = 0;
                dos_ctime = 0;
            }
            else
            {
                timeinfo = localtime(&(file_stat.st_ctim.tv_sec));
                dos_cdate = DIR_MAKE_FILE_DATE((timeinfo->tm_year + 1900), (timeinfo->tm_mon + 1), timeinfo->tm_mday);
                dos_ctime = DIR_MAKE_FILE_TIME(timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
                dbg_printf("system_go_examine_directory defined file cdate/time: %s", asctime(timeinfo));
            }

            if (file_stat.st_atim.tv_sec == 0)
            {
                dos_adate = 0;
            }
            else
            {
                timeinfo = localtime(&(file_stat.st_atim.tv_sec));
                dos_adate = DIR_MAKE_FILE_DATE((timeinfo->tm_year + 1900), (timeinfo->tm_mon + 1), timeinfo->tm_mday);
                dbg_printf("system_go_examine_directory defined file adate/time: %s", asctime(timeinfo));
            }

            if (file_stat.st_mtim.tv_sec == 0)
            {
                dos_mdate = 0;
                dos_mtime = 0;
            }
            else
            {
                timeinfo = localtime(&(file_stat.st_mtim.tv_sec));
                dos_mdate = DIR_MAKE_FILE_DATE((timeinfo->tm_year + 1900), (timeinfo->tm_mon + 1), timeinfo->tm_mday);
                dos_mtime = DIR_MAKE_FILE_TIME(timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
                dbg_printf("system_go_examine_directory defined file mdate/time: %s", asctime(timeinfo));
            }

            dir_info->DIR_NTRes = 0;
            dir_info->DIR_CrtTimeTenth = 0;
            dir_info->DIR_CrtTime[0] = (dos_ctime & 0x00ff);
            dir_info->DIR_CrtTime[1] = (dos_ctime >> 8);
            dir_info->DIR_CrtDate[0] = (dos_cdate & 0x00ff);
            dir_info->DIR_CrtDate[1] = (dos_cdate >> 8);
            dir_info->DIR_LstAccDate[0] = (dos_adate & 0x00ff);
            dir_info->DIR_LstAccDate[1] = (dos_adate >> 8);
            dir_info->DIR_FstClusHI[0] = 0;
            dir_info->DIR_FstClusHI[1] = 0;
            dir_info->DIR_WrtTime[0] = (dos_mtime & 0x00ff);
            dir_info->DIR_WrtTime[1] = (dos_mtime >> 8);
            dir_info->DIR_WrtDate[0] = (dos_mdate & 0x00ff);
            dir_info->DIR_WrtDate[1] = (dos_mdate >> 8);
            dir_info->DIR_FstClusLO[0] = 0;
            dir_info->DIR_FstClusLO[1] = 0;
            dir_info->DIR_FileSize[0] = (file_stat.st_size & 0x000000ff) >>  0;
            dir_info->DIR_FileSize[1] = (file_stat.st_size & 0x0000ff00) >>  8;
            dir_info->DIR_FileSize[2] = (file_stat.st_size & 0x00ff0000) >> 16;
            dir_info->DIR_FileSize[3] = (file_stat.st_size & 0xff000000) >> 24;

            is_done = DOS_TRUE;
        }
    }

    if(old_dir)
    {
            chdir(old_dir);
            system_free_mem(old_dir);
    }

    return is_done;
}

void system_finish_examine_directory(DOS_CONTEXT *context, DOS_DIR fib)
{
    if (fib)
    {
        closedir(fib->handle);
        system_free_mem(fib);
    }
}

DOS_S32 system_get_file_size(DOS_CONTEXT *context, DOS_FILE file)
{
    DOS_S32 file_size = 0xffffffff;
    struct stat file_stat;

    if (file)
    {
        if ( !fstat(fileno(file->file), &file_stat) )
        {
            file_size = file_stat.st_size;
        }
        else
        {
            dbg_printf("system_get_file_size: error\n");
        }
    }
    else
        dbg_printf("system_get_file_size: no file handle\n");

    return file_size;
}

/* /// */

