
/* /// "Windows system functions" */

DOS_APTR system_alloc_mem(int size)
{
    return GlobalAlloc(GMEM_FIXED, size);
}

void system_free_mem(DOS_APTR ptr)
{
    GlobalFree(ptr);
}

DOS_BOOL system_init_context(DOS_CONTEXT *context, UNUSED DOS_APTR user_data)
{
    // Nothing to do?
    return DOS_TRUE;
};

void system_clean_context(DOS_CONTEXT *context)
{
    // Nothing to do?
}

DOS_BOOL system_get_disk_info(DOS_CONTEXT *context, DOS_LOCK root_lock, struct DiskQuery *disk_info)
{
    BOOL got_info = FALSE;
    char volume[4];
    DWORD sectors_per_cluster;
    DWORD bytes_per_sector;
    DWORD number_of_free_clusters;
    DWORD total_number_of_clusters;

    //@iss volume[0] = (char)(PathGetDriveNumber(root_lock->path) + 'A');
    volume[0] = ((char*)root_lock->path)[0] & 0xdf;
    volume[1] = ':';
    volume[2] = '\\';
    volume[3] = '\0';

    if(GetDiskFreeSpace(volume, &sectors_per_cluster, &bytes_per_sector, &number_of_free_clusters, &total_number_of_clusters))
    {
        INT64 total_sector = (total_number_of_clusters * sectors_per_cluster * bytes_per_sector) / 512;
        INT64 free_sector = (number_of_free_clusters * sectors_per_cluster * bytes_per_sector) / 512;

        disk_info->DISK_TotalSector[0] = (INT8)((total_sector & 0x000000ff) >>  0);
        disk_info->DISK_TotalSector[1] = (INT8)((total_sector & 0x0000ff00) >>  8);
        disk_info->DISK_TotalSector[2] = (INT8)((total_sector & 0x00ff0000) >> 16);
        disk_info->DISK_TotalSector[3] = (INT8)((total_sector & 0xff000000) >> 24);

        disk_info->DISK_FreeSector[0] = (INT8)((free_sector & 0x000000ff) >>  0);
        disk_info->DISK_FreeSector[1] = (INT8)((free_sector & 0x0000ff00) >>  8);
        disk_info->DISK_FreeSector[2] = (INT8)((free_sector & 0x00ff0000) >> 16);
        disk_info->DISK_FreeSector[3] = (INT8)((free_sector & 0xff000000) >> 24);

        disk_info->DISK_DiskFat = 0x0c; // FAT32?

        got_info = TRUE;
    }
    return got_info;
}

DOS_BOOL system_directory_update(DOS_CONTEXT *context, DOS_LOCK dir_lock, struct FatDirInfo *dir_info)
{
	// TODO
}

DOS_LOCK system_obtain_directory_lock(DOS_CONTEXT *context, const DOS_TEXT *dir_path, DOS_LOCK root_lock)
{
    DOS_LOCK lock = NULL;
    TCHAR old_dir[MAX_PATH];
    struct stat s;

    if(root_lock)
    {
        GetCurrentDirectory(sizeof(old_dir), old_dir);
        SetCurrentDirectory(root_lock->path);
    };

    //@iss PathIsDirectory(dir_path)
    if(stat(dir_path,&s) == 0 && s.st_mode & S_IFDIR)
    {
        lock = (DOS_LOCK)system_alloc_mem(sizeof(struct _DOS_LOCK ));
        GetFullPathName(dir_path, MAX_PATH, lock->path, NULL);
        lock->handle = INVALID_HANDLE_VALUE;
    }

    if(root_lock)
    {
        SetCurrentDirectory(old_dir);
    };

    return lock;
}

void system_release_directory_lock(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    if(dir_lock != (DOS_LOCK)0)
    {
        system_free_mem(dir_lock);
    }
}

DOS_LOCK system_clone_directory_lock(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    DOS_LOCK dup_lock = (DOS_LOCK)system_alloc_mem(sizeof(struct _DOS_LOCK));

    CopyMemory(dup_lock, dir_lock, sizeof(struct _DOS_LOCK));

    return dup_lock;
}

static HANDLE file_open(DOS_CONTEXT *context, const DOS_TEXT *file_name, PCHAR root_path, DWORD creation_disposition)
{
    TCHAR old_dir[MAX_PATH];
    HANDLE file;

    if(root_path)
    {
        GetCurrentDirectory(sizeof(old_dir), old_dir);
        SetCurrentDirectory(root_path);
    }

    file = CreateFile(file_name, GENERIC_READ | GENERIC_WRITE, 0, NULL, creation_disposition, FILE_ATTRIBUTE_NORMAL, NULL);

    if(root_path)
    {
        SetCurrentDirectory(old_dir);
    }

    if(file != INVALID_HANDLE_VALUE)
        return file;
    else
        return NULL;
}

DOS_BOOL system_file_delete(DOS_CONTEXT *context, const DOS_TEXT *file_name, DOS_LOCK root_lock)
{
    LONG err;
    TCHAR old_dir[MAX_PATH];

    if (root_lock)
    {
        GetCurrentDirectory(sizeof(old_dir), old_dir);
        SetCurrentDirectory(root_lock->path);
    }

    dbg_printf("system_file_delete: %s\n", file_name);

    err = DeleteFile(file_name);

    if (root_lock)
    {
        SetCurrentDirectory(old_dir);
    }

    return (err != 0);
}

DOS_BOOL system_directory_delete(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    dbg_printf("system_directory_delete: %p\n", dir_lock);

    if (dir_lock)
    {
        if(RemoveDirectory((LPCSTR)dir_lock) != 0)
        {
            system_release_directory_lock(dir_lock);
            return DOS_TRUE;
        }
    }

    return DOS_FALSE;
}

DOS_LOCK system_create_directory(DOS_CONTEXT *context, const DOS_TEXT *dir_path, DOS_LOCK root_lock)
{
    TCHAR old_dir[MAX_PATH];
    DOS_LOCK lock = NULL;

    if (root_lock)
    {
        GetCurrentDirectory(sizeof(old_dir), old_dir);
        SetCurrentDirectory(root_lock->path);
    };

    dbg_printf("system_create_directory: %s\n", dir_path);

    if (CreateDirectory(dir_path, NULL))
    {
        lock = (DOS_LOCK)system_alloc_mem(sizeof(struct _DOS_LOCK ));
        SetCurrentDirectory(dir_path);
        GetCurrentDirectory(sizeof(lock->path), lock->path);
        lock->handle = INVALID_HANDLE_VALUE;
    }

    if (root_lock)
    {
        SetCurrentDirectory(old_dir);
    };

    return lock;
}



DOS_FILE system_file_open_existing(DOS_CONTEXT *context, const DOS_TEXT *file_name, DOS_LOCK root_lock)
{
    return file_open(context, file_name, root_lock->path, OPEN_EXISTING);
}

DOS_FILE system_file_open_new(DOS_CONTEXT *context, const DOS_TEXT *file_name, DOS_LOCK root_lock)
{
    return file_open(context, file_name, root_lock->path, CREATE_ALWAYS);
}

void system_file_close(DOS_CONTEXT *context, DOS_FILE file)
{
    if(file) CloseHandle(file);
}

DOS_S32 system_file_seek(DOS_CONTEXT *context, DOS_FILE file, int pos)
{
    if(file != NULL)
        return SetFilePointer(file, pos, NULL, FILE_BEGIN);
    else
        return -1;
}

DOS_S32 system_file_read(DOS_CONTEXT *context, DOS_FILE file, DOS_APTR buffer, DOS_S32 size)
{
    DWORD read_bytes;

    if(ReadFile(file, buffer, size, &read_bytes, NULL))
        return read_bytes;
    else
        return -1;
}

DOS_S32 system_file_write(DOS_CONTEXT *context, DOS_FILE file, DOS_APTR buffer, DOS_S32 size)
{
    DWORD written_bytes;

    if(WriteFile(file, buffer, size, &written_bytes, NULL))
        return written_bytes;
    else
        return -1;
}

DOS_DIR system_start_examine_directory(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    LPWIN32_FIND_DATA find_file_data = (LPWIN32_FIND_DATA)system_alloc_mem(sizeof(WIN32_FIND_DATA));
    TCHAR szDir[MAX_PATH];
    StringCchCopy(szDir, MAX_PATH, dir_lock->path);
    StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

    dir_lock->handle = FindFirstFile(szDir, find_file_data);

    if(dir_lock->handle == INVALID_HANDLE_VALUE)
    {
        system_free_mem(find_file_data);
        find_file_data = NULL;
    }

    return find_file_data;
}

DOS_BOOL system_go_examine_directory(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_DIR fib, struct FatDirInfo *dir_info, DOS_TEXT *pattern)
{
    BOOL is_done = FALSE;

    if(dir_lock->handle != INVALID_HANDLE_VALUE)
    {
        while(!is_done && FindNextFile(dir_lock->handle, fib))
        {
            if(normalize_file_name(fib->cFileName, dir_info->DIR_Name) && pattern_match(pattern, dir_info->DIR_Name))
            {
                // Storing attributes :)
                dir_info->DIR_Attr = (UINT8)fib->dwFileAttributes;

                dbg_printf("system_go_examine_directory defined file attributes: %d\n", dir_info->DIR_Attr);

                dir_info->DIR_NTRes = 0;
                dir_info->DIR_CrtTimeTenth = 0;
                dir_info->DIR_CrtTime[0] = 0;
                dir_info->DIR_CrtTime[1] = 0;
                dir_info->DIR_CrtDate[0] = 0;
                dir_info->DIR_CrtDate[1] = 0;
                dir_info->DIR_LstAccDate[0] = 0;
                dir_info->DIR_LstAccDate[1] = 0;
                dir_info->DIR_FstClusHI[0] = 0;
                dir_info->DIR_FstClusHI[1] = 0;
                dir_info->DIR_WrtTime[0] = 0;
                dir_info->DIR_WrtTime[1] = 0;
                dir_info->DIR_WrtDate[0] = 0;
                dir_info->DIR_WrtDate[1] = 0;
                dir_info->DIR_FstClusLO[0] = 0;
                dir_info->DIR_FstClusLO[1] = 0;
                dir_info->DIR_FileSize[0] = (UINT8)((fib->nFileSizeLow & 0x000000ff) >>  0);
                dir_info->DIR_FileSize[1] = (UINT8)((fib->nFileSizeLow & 0x0000ff00) >>  8);
                dir_info->DIR_FileSize[2] = (UINT8)((fib->nFileSizeLow & 0x00ff0000) >> 16);
                dir_info->DIR_FileSize[3] = (UINT8)((fib->nFileSizeLow & 0xff000000) >> 24);

                is_done = TRUE;
            }
        }
    }

    return is_done;
}

void system_finish_examine_directory(DOS_CONTEXT *context, DOS_DIR fib)
{
    if(fib)
    {
        FindClose(fib);
        system_free_mem(fib);
    }
}

DOS_S32 system_get_file_size(DOS_CONTEXT *context, DOS_FILE file)
{
    DOS_S32 file_size = 0xffffffff;
    BY_HANDLE_FILE_INFORMATION file_stat;

    if (file)
    {
        if ( GetFileInformationByHandle(file, &file_stat) )
        {
            // Only low DWORD
            file_size = file_stat.nFileSizeLow;
        }
        else
        {
           dbg_printf("system_get_file_size: error\n");
        }
    }
    else
        dbg_printf("system_get_file_size: no file handle\n");

    return file_size;
}

/* /// */

