/********************************************************************
 *                                                                  *
 * Portable DOS (29.12.2022)                                        *
 *                                                                  *
 * Code:                                                            *
 *   J�r�me 'Jede' Debrune                                          *
 *   Philippe 'OffseT' Rimauro                                      *
 *   Christian 'Assinie' Lardi�re                                   *
 *                                                                  *
 ** ch376.c *********************************************************/

/* /// "Amiga system functions" */

/* Private */

#define DISABLE_DOSREQ() \
{ \
    struct Process *process = (struct Process *)FindTask(NULL); \
    APTR old_windowPtr = process->pr_WindowPtr; \
    process->pr_WindowPtr = (APTR)-1; \
    do {} while(0)

#define ENABLE_DOSREQ() \
    process->pr_WindowPtr = old_windowPtr; \
} \
do {} while(0)

static UWORD fatDate(DOS_CONTEXT *context, const struct DateStamp *time)
{
    struct Library *UtilityBase = context->UtilityBase;
    struct ClockData c;
    ULONG s = (time->ds_Days * 24 * 60 + time->ds_Minute) * 60 + time->ds_Tick / 50;

    Amiga2Date(s, &c);

    return DIR_MAKE_FILE_DATE(c.year, c.month, c.mday);
}

static UWORD fatTime(UNUSED DOS_CONTEXT *context, const struct DateStamp *time)
{
    UBYTE h = time->ds_Minute / 60;
    UBYTE m = time->ds_Minute % 60;
    UBYTE s = time->ds_Tick / 50;

    return DIR_MAKE_FILE_TIME(h, m, s);
}

static BOOL priv_feed_dir_info(DOS_CONTEXT *context, struct FileInfoBlock *fib, struct FatDirInfo *dir_info)
{
    if(normalize_file_name(fib->fib_FileName, dir_info->DIR_Name))
    {
        UWORD time = fatTime(context, &fib->fib_Date);
        UWORD date = fatDate(context, &fib->fib_Date);

        dir_info->DIR_Attr = 0;

        // Note: on Amiga flags are protections! (active at 0)
        if(fib->fib_DirEntryType > 0)
            dir_info->DIR_Attr |= DIR_ATTR_DIRECTORY;
        if((fib->fib_Protection & (FIBF_WRITE | FIBF_DELETE)) != 0)
            dir_info->DIR_Attr |= DIR_ATTR_READ_ONLY;
        if((fib->fib_Protection & FIBF_ARCHIVE) == 0)
            dir_info->DIR_Attr |= DIR_ATTR_ARCHIVE;
        // Hidden files do not exist on Amiga,
        // but this flag is commonly used for this purpose
        if((fib->fib_Protection & FIBF_HOLD) != 0)
            dir_info->DIR_Attr |= DIR_ATTR_HIDDEN;
        if((fib->fib_Protection & FIBF_PURE) != 0)
            dir_info->DIR_Attr |= DIR_ATTR_SYSTEM;
        //if(fib->fib_Protection & FIBF_EXECUTE)
        //if(fib->fib_Protection & FIBF_READ)
        //if(fib->fib_Protection & FIBF_SCRIPT)
        dbg_printf("priv_feed_dir_info defined file attributes: %d\n", dir_info->DIR_Attr);

        dir_info->DIR_NTRes = 0;
        dir_info->DIR_CrtTimeTenth = 0;
        dir_info->DIR_CrtTime[0] = (time & 0x00ff) >> 0;
        dir_info->DIR_CrtTime[1] = (time & 0xff00) >> 8;
        dir_info->DIR_CrtDate[0] = (date & 0x00ff) >> 0;
        dir_info->DIR_CrtDate[1] = (date & 0xff00) >> 8;
        dir_info->DIR_LstAccDate[0] = dir_info->DIR_CrtDate[0];
        dir_info->DIR_LstAccDate[1] = dir_info->DIR_CrtDate[1];
        dir_info->DIR_FstClusHI[0] = 0;
        dir_info->DIR_FstClusHI[1] = 0;
        dir_info->DIR_WrtTime[0] = dir_info->DIR_CrtTime[0];
        dir_info->DIR_WrtTime[1] = dir_info->DIR_CrtTime[1];
        dir_info->DIR_WrtDate[0] = dir_info->DIR_CrtDate[0];
        dir_info->DIR_WrtDate[1] = dir_info->DIR_CrtDate[1];
        dir_info->DIR_FstClusLO[0] = 0;
        dir_info->DIR_FstClusLO[1] = 0;
        dir_info->DIR_FileSize[0] = (fib->fib_Size & 0x000000ff) >>  0;
        dir_info->DIR_FileSize[1] = (fib->fib_Size & 0x0000ff00) >>  8;
        dir_info->DIR_FileSize[2] = (fib->fib_Size & 0x00ff0000) >> 16;
        dir_info->DIR_FileSize[3] = (fib->fib_Size & 0xff000000) >> 24;

        return TRUE;
    }

    return FALSE;
}

static BOOL priv_entry_update(DOS_CONTEXT *context, STRPTR entry_full_path, struct FatDirInfo *dir_info)
{
    // We only handle name and protection bits changes (date change is still to be done and cluster rewrite won't work)
    struct Library *DOSBase = context->DOSBase;
    struct Library *UtilityBase = context->UtilityBase;
    BPTR dir_lock;
    STRPTR old_name = system_clone_string(FilePart(entry_full_path));
    STRPTR curr_path = system_clone_string(entry_full_path);
    BOOL is_done = FALSE;

    dbg_printf("priv_entry_update: %s\n", entry_full_path);

    if(old_name && curr_path)
    {
        STRPTR curr_path_end = PathPart(curr_path);
        TEXT new_name[16];
        TEXT fixed_new_name[13]; // Max = 8 + '.' + 3 + '\0'
        LONG i,j;

        *curr_path_end = '\0';

        for(i=0,j=0; i<8; i++) new_name[j++] = dir_info->DIR_Name[i];
        new_name[j++] = '.';
        for(;i<11; i++) new_name[j++] = dir_info->DIR_Name[i];
        new_name[j++] = '\0';

        system_trim_file_name(new_name, fixed_new_name);

        dir_lock = Lock(curr_path, SHARED_LOCK);

        if(dir_lock)
        {
            BPTR old_dir = CurrentDir(dir_lock);

            if(Rename(old_name, fixed_new_name))
            {
                // Note: on Amiga flags are protections! (active at 0)
                UBYTE protection = FIBF_EXECUTE | FIBF_ARCHIVE;
                UWORD d = dir_info->DIR_WrtDate[0] | (dir_info->DIR_WrtDate[1] << 8);
                UWORD t = dir_info->DIR_WrtTime[0] | (dir_info->DIR_WrtTime[1] << 8);
                struct ClockData c;
                struct DateStamp ds;
                ULONG seconds;

            	c.mday = DIR_GET_DAY(d);
            	c.month = DIR_GET_MONTH(d);
            	c.year = DIR_GET_YEAR(d);
            	c.hour = DIR_GET_HOURS(t);
            	c.min = DIR_GET_MINS(t);
            	c.sec = DIR_GET_SECS(t);

                seconds = Date2Amiga(&c);

                ds.ds_Days = seconds / (60 * 60 * 24);
                ds.ds_Minute = (seconds / 60) % (60 * 24);
                ds.ds_Tick = 50 * (seconds % 60);

                //dbg_printf("priv_entry_update time,date: 0x%04x,0x%04x\n", t, d);
                //dbg_printf("priv_entry_update clock    : %d/%d/%d, %d:%d:%d\n", c.mday, c.month, c.year, c.hour, c.min, c.sec);
                //dbg_printf("priv_entry_update seconds  : %d\n", seconds);
                //dbg_printf("priv_entry_update DateStamp: %d,%d,%d\n", ds.ds_Days, ds.ds_Minute, ds.ds_Tick);

                if(dir_info->DIR_Attr & DIR_ATTR_READ_ONLY)
                    protection |= FIBF_WRITE | FIBF_DELETE;
                // Hidden files do not exist on Amiga,
                // but this flag is commonly used for this purpose
                if(dir_info->DIR_Attr & DIR_ATTR_HIDDEN)
                    protection |= FIBF_HOLD;
                if(dir_info->DIR_Attr & DIR_ATTR_ARCHIVE)
                    protection &= ~FIBF_ARCHIVE;
                if(dir_info->DIR_Attr & DIR_ATTR_SYSTEM)
                    protection |= FIBF_PURE;
                //if(dir_info->DIR_Attr & DIR_ATTR_VOLUME_ID)
                //if(dir_info->DIR_Attr & DIR_ATTR_DIRECTORY)

                if(SetProtection(fixed_new_name, protection) && SetFileDate(fixed_new_name, &ds))
                {
                    is_done = TRUE;
                }
            }

            CurrentDir(old_dir);

            UnLock(dir_lock);
        }

        system_free_mem(old_name);
        system_free_mem(curr_path);
    }

    return is_done;
}

static BOOL priv_directory_delete(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    if(dir_lock)
    {
        struct Library *DOSBase = context->DOSBase;
        TEXT dir_path[1024];
        BOOL got_dir_path = NameFromLock(dir_lock, dir_path, sizeof(dir_path));

        dbg_printf("priv_directory_delete: %p\n", dir_lock);

        UnLock(dir_lock);

        if(got_dir_path)
        {
            SetProtection(dir_path, 0);

            if(DeleteFile(dir_path))
            {
                return TRUE;
            }
        }
    }

    return FALSE;
}

/* Public */

DOS_APTR system_alloc_mem(int size)
{
    return AllocVec(size, MEMF_PUBLIC);
}

void system_free_mem(DOS_APTR ptr)
{
    FreeVec(ptr);
}

DOS_BOOL system_init_context(DOS_CONTEXT *context, UNUSED DOS_APTR user_data)
{
    context->DOSBase = OpenLibrary(DOSNAME, 0L);
    context->UtilityBase = OpenLibrary(UTILITYNAME, 0L);

    return context->DOSBase != NULL && context->UtilityBase != NULL;
}

void system_clean_context(DOS_CONTEXT *context)
{
    CloseLibrary(context->DOSBase);
    CloseLibrary(context->UtilityBase);
}

DOS_BOOL system_is_root_dir(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_LOCK root_dir)
{
    struct Library *DOSBase = context->DOSBase;

    return SameLock(dir_lock, root_dir) == LOCK_SAME;
}

DOS_BOOL system_get_disk_info(DOS_CONTEXT *context, DOS_LOCK root_lock, struct DiskQuery *disk_info)
{
    struct Library *DOSBase = context->DOSBase;
    BOOL got_info = DOS_FALSE;

    if(disk_info)
    {
        struct InfoData *info = system_alloc_mem(sizeof(struct InfoData));

        if(info)
        {
            if(Info(root_lock, info) == DOSTRUE)
            {
                ULONG total_sector = ((QUAD)info->id_NumBlocks * (QUAD)info->id_BytesPerBlock) / 512;
                ULONG free_sector = (((QUAD)info->id_NumBlocks - (QUAD)info->id_NumBlocksUsed) * (QUAD)info->id_BytesPerBlock) / 512;

                disk_info->DISK_TotalSector[0] = (total_sector & 0x000000ff) >>  0;
                disk_info->DISK_TotalSector[1] = (total_sector & 0x0000ff00) >>  8;
                disk_info->DISK_TotalSector[2] = (total_sector & 0x00ff0000) >> 16;
                disk_info->DISK_TotalSector[3] = (total_sector & 0xff000000) >> 24;

                disk_info->DISK_FreeSector[0] = (free_sector & 0x000000ff) >>  0;
                disk_info->DISK_FreeSector[1] = (free_sector & 0x0000ff00) >>  8;
                disk_info->DISK_FreeSector[2] = (free_sector & 0x00ff0000) >> 16;
                disk_info->DISK_FreeSector[3] = (free_sector & 0xff000000) >> 24;

                disk_info->DISK_DiskFat = 0x0c; // FAT32?

                got_info = TRUE;
            }
            system_free_mem(info);
        }
    }

    return got_info;
}

DOS_BOOL system_file_update(DOS_CONTEXT *context, DOS_FILE file, struct FatDirInfo *dir_info)
{
    struct Library *DOSBase = context->DOSBase;
    BOOL is_done = FALSE;

    dbg_printf("system_file_update: %p\n", file);

    if(file)
    {
        TEXT file_path[1024];
        BOOL got_file_path = NameFromFH(file, file_path, sizeof(file_path));

        if(got_file_path)
        {
            is_done = priv_entry_update(context, file_path, dir_info);
        }
    }
           
    return is_done;
}

DOS_BOOL system_directory_update(DOS_CONTEXT *context, DOS_LOCK dir_lock, struct FatDirInfo *dir_info)
{
    struct Library *DOSBase = context->DOSBase;
    BOOL is_done = FALSE;

    dbg_printf("system_file_update: %p\n", dir_lock);

    if(dir_lock)
    {
        TEXT dir_path[1024];
        BOOL got_dir_path = NameFromLock(dir_lock, dir_path, sizeof(dir_path));

        if(got_dir_path)
        {
            is_done = priv_entry_update(context, dir_path, dir_info);
        }
    }

    return is_done;
}

DOS_LOCK system_obtain_directory_lock(DOS_CONTEXT *context, const DOS_TEXT *dir_path, DOS_LOCK root_lock)
{
    struct Library *DOSBase = context->DOSBase;
    BPTR lock = (BPTR)0;

    dbg_printf("system_obtain_directory_lock: %s\n", dir_path);

    if(dir_path)
    {
        DISABLE_DOSREQ();

        BPTR old_lock = (BPTR)0;

        if(root_lock)
        {
            old_lock = CurrentDir(root_lock);
        }

        lock = Lock(dir_path, SHARED_LOCK);

        if(lock != (BPTR)0)
        {
            struct FileInfoBlock *fib = AllocDosObject(DOS_FIB, NULL);

            if(fib)
            {
                if(Examine(lock, fib) == DOSFALSE || fib->fib_DirEntryType < 0)
                {
                    UnLock(lock);
                    lock = (BPTR)0;
                }
                FreeDosObject(DOS_FIB, fib);
            }
        }

        if(old_lock)
        {
            CurrentDir(old_lock);
        }

        ENABLE_DOSREQ();
    }

    return lock;
}

void system_release_directory_lock(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    struct Library *DOSBase = context->DOSBase;
    UnLock(dir_lock);
}

DOS_LOCK system_clone_directory_lock(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    struct Library *DOSBase = context->DOSBase;

    if(dir_lock)
    {
        return DupLock(dir_lock);
    }
    else
    {
        return dir_lock;
    }
}

DOS_LOCK system_create_directory(DOS_CONTEXT *context, const DOS_TEXT *dir_path, DOS_LOCK root_lock)
{
    struct Library *DOSBase = context->DOSBase;
    BPTR lock;
    BPTR old_lock = (BPTR)0;

    DISABLE_DOSREQ();

    dbg_printf("system_create_directory: %s\n", dir_path);

    if(root_lock)
    {
        old_lock = CurrentDir(root_lock);
    }

    lock = CreateDir(dir_path);

    if(old_lock)
    {
        CurrentDir(old_lock);
    }

    ENABLE_DOSREQ();

    return lock;
}

static BPTR file_open(DOS_CONTEXT *context, CONST_STRPTR file_name, BPTR root_lock, LONG mode)
{
    struct Library *DOSBase = context->DOSBase;
    BPTR old_lock = (BPTR)0;
    BPTR file;

    DISABLE_DOSREQ();

    if(root_lock)
    {
        old_lock = CurrentDir(root_lock);
    }

    file = Open(file_name, mode);

    if(old_lock)
    {
        CurrentDir(old_lock);
    }

    ENABLE_DOSREQ();

    return file;
}

DOS_FILE system_file_open_existing(DOS_CONTEXT *context, const DOS_TEXT *file_name, DOS_LOCK root_lock)
{
    return file_open(context, file_name, root_lock, MODE_OLDFILE);
}

DOS_FILE system_file_open_new(DOS_CONTEXT *context, const DOS_TEXT *file_name, DOS_LOCK root_lock)
{
    return file_open(context, file_name, root_lock, MODE_NEWFILE);
}

void system_file_close(DOS_CONTEXT *context, DOS_FILE file)
{
    struct Library *DOSBase = context->DOSBase;
    Close(file);
}

DOS_S32 system_file_seek(DOS_CONTEXT *context, DOS_FILE file, int pos)
{
    struct Library *DOSBase = context->DOSBase;

    dbg_printf("system_file_seek trying to seek to position %d\n", pos);

    if(file)
        return Seek(file, pos, OFFSET_BEGINNING);
    else
        return -1;
}

DOS_S32 system_file_set_size(DOS_CONTEXT *context, DOS_FILE file)
{
    struct Library *DOSBase = context->DOSBase;

    dbg_printf("system_file_set_size trying to set new file size\n");

    if(file)
        return SetFileSize(file, Seek(file, 0, OFFSET_CURRENT), OFFSET_BEGINNING);
    else
        return -1;
}

DOS_S32 system_file_read(DOS_CONTEXT *context, DOS_FILE file, DOS_APTR buffer, DOS_S32 size)
{
    struct Library *DOSBase = context->DOSBase;

    dbg_printf("system_file_read trying to read %d bytes\n", size);

    if(file)
        return Read(file, buffer, size);
    else
        return -1;
}

DOS_S32 system_file_write(DOS_CONTEXT *context, DOS_FILE file, DOS_APTR buffer, DOS_S32 size)
{
    struct Library *DOSBase = context->DOSBase;

    dbg_printf("system_file_write trying to write %d bytes\n", size);

    if(file)
        return Write(file, buffer, size);
    else
        return -1;
}

DOS_BOOL system_file_delete(DOS_CONTEXT *context, DOS_FILE file)
{
    struct Library *DOSBase = context->DOSBase;

    dbg_printf("system_file_delete: %p\n", file);

    if(file)
    {
        TEXT file_path[1024];
        BOOL got_file_path = NameFromFH(file, file_path, sizeof(file_path));

        Close(file);

        if(got_file_path)
        {
            SetProtection(file_path, 0);

            if(DeleteFile(file_path))
            {
                return DOS_TRUE;
            }
        }
    }

    return DOS_FALSE;
}

DOS_BOOL system_directory_delete(DOS_CONTEXT *context, DOS_LOCK dir_lock)
{
    struct Library *DOSBase = context->DOSBase;
    BOOL success = DOS_TRUE;

    dbg_printf("system_directory_delete: %p\n", dir_lock);

    if(dir_lock)
    {
        struct FileInfoBlock *fib = AllocDosObject(DOS_FIB, NULL);

        if(fib)
        {
            BPTR oldDir = CurrentDir(dir_lock);

            // First recursively delete directory contents
            if(Examine(dir_lock, fib) == DOSTRUE)
            {
                while(ExNext(dir_lock, fib) == DOSTRUE)
                {
                    //Directory?
                    if(fib->fib_DirEntryType > 0)
                    {
                        BPTR lock = Lock(fib->fib_FileName, SHARED_LOCK);

                        if(lock)
                        {
                            // This will actually unlock directory too
                            if(!system_directory_delete(context, lock))
                            {
                                success = DOS_FALSE;
                            }
                        }
                        else
                        {
                            success = DOS_FALSE;
                        }
                    }
                    else
                    {
                        SetProtection(fib->fib_FileName, 0);

                        if(!DeleteFile(fib->fib_FileName))
                        {
                            success = DOS_FALSE;
                        }
                    }
                }
            }
            FreeDosObject(DOS_FIB, fib);
            CurrentDir(oldDir);
        }

        // Actually delete the directory from its lock
        if(!priv_directory_delete(context, dir_lock))
        {
            success = DOS_FALSE;
        }
    }

    return success;
}

DOS_BOOL system_file_info_read(DOS_CONTEXT *context, DOS_FILE file, struct FatDirInfo *dir_info)
{
    struct Library *DOSBase = context->DOSBase;
    BOOL is_done = FALSE;

    dbg_printf("system_file_read_info: %p\n", file);

    if(file)
    {
        struct FileInfoBlock *fib = AllocDosObject(DOS_FIB, NULL);

        if(fib)
        {
            if(ExamineFH(file, fib) == DOSTRUE)
            {
                is_done = priv_feed_dir_info(context, fib, dir_info);
            }

            FreeDosObject(DOS_FIB, fib);
        }

    }

    return is_done;
}

DOS_BOOL system_directory_info_read(DOS_CONTEXT *context, DOS_LOCK dir_lock, struct FatDirInfo *dir_info)
{
    struct Library *DOSBase = context->DOSBase;
    BOOL is_done = FALSE;

    dbg_printf("system_directory_read_info: %p\n", dir_lock);

    if(dir_lock)
    {
        struct FileInfoBlock *fib = AllocDosObject(DOS_FIB, NULL);

        if(fib)
        {
            if(Examine(dir_lock, fib) == DOSTRUE)
            {
                is_done = priv_feed_dir_info(context, fib, dir_info);
            }

            FreeDosObject(DOS_FIB, fib);
        }

    }

    return is_done;
}

DOS_DIR system_start_examine_directory(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_TEXT *pattern)
{
    struct Library *DOSBase = context->DOSBase;
    DOS_DIR dir = system_alloc_mem(sizeof(struct _DOS_DIR));

    if(dir)
    {
        dir->fib = AllocDosObject(DOS_FIB, NULL);

        if(dir->fib)
        {
            if(Examine(dir_lock, dir->fib) == DOSTRUE)
            {
                normalize_pattern(pattern, dir->pattern);
                dir->ExNextStep = 2;
            }
            else
            {
                FreeDosObject(DOS_FIB, dir->fib);
                system_free_mem(dir);
                dir = NULL;
            }
        }
        else
        {
            system_free_mem(dir);
            dir = NULL;
        }
    }

    return dir;
}

DOS_BOOL system_go_examine_directory(DOS_CONTEXT *context, DOS_LOCK dir_lock, DOS_DIR dir, struct FatDirInfo *dir_info)
{
    struct Library *DOSBase = context->DOSBase;
    BOOL is_done = FALSE;

    if(dir)
    {
        if(dir->ExNextStep)
        {
            // Add weird '.' and '..' entries
            STRPTR fakedEntry;

            if(dir->ExNextStep == 2)
                fakedEntry = ".          ";
            else
                fakedEntry = "..         ";

            dir->ExNextStep--;

            CopyMem(fakedEntry, dir_info->DIR_Name, 11);

            dir_info->DIR_Attr = DIR_ATTR_DIRECTORY;
            dir_info->DIR_NTRes = 0;
            dir_info->DIR_CrtTimeTenth = 0;
            dir_info->DIR_CrtTime[0] = 0;
            dir_info->DIR_CrtTime[1] = 0;
            dir_info->DIR_CrtDate[0] = 0;
            dir_info->DIR_CrtDate[1] = 0;
            dir_info->DIR_LstAccDate[0] = 0;
            dir_info->DIR_LstAccDate[1] = 0;
            dir_info->DIR_FstClusHI[0] = 0;
            dir_info->DIR_FstClusHI[1] = 0;
            dir_info->DIR_WrtTime[0] = 0;
            dir_info->DIR_WrtTime[1] = 0;
            dir_info->DIR_WrtDate[0] = 0;
            dir_info->DIR_WrtDate[1] = 0;
            dir_info->DIR_FstClusLO[0] = 0;
            dir_info->DIR_FstClusLO[1] = 0;
            dir_info->DIR_FileSize[0] = 0;
            dir_info->DIR_FileSize[1] = 0;
            dir_info->DIR_FileSize[2] = 0;
            dir_info->DIR_FileSize[3] = 0;

            is_done = TRUE;
        }
        else if(dir->fib) while(!is_done && ExNext(dir_lock, dir->fib) == DOSTRUE)
        {
            LONG i;

            for(i=0; dir->fib->fib_FileName[i] != '\0'; i++);

            // Exclude files with a ending '.' (files with no extension shall not have any traling '.')
            if(dir->fib->fib_FileName[i-1] != '.')
            {
                if(priv_feed_dir_info(context, dir->fib, dir_info))
                {
                    is_done = pattern_match(dir->pattern, dir_info->DIR_Name);
                }
            }
        }
    }

    return is_done;
}

void system_finish_examine_directory(DOS_CONTEXT *context, DOS_DIR dir)
{
    struct Library *DOSBase = context->DOSBase;

    if(dir)
    {
        if(dir->fib)
        {
            FreeDosObject(DOS_FIB, dir->fib);
        }

        system_free_mem(dir);
    }
}

DOS_S32 system_get_file_size(DOS_CONTEXT *context, DOS_FILE file)
{
    struct Library *DOSBase = context->DOSBase;
    struct FileInfoBlock *fib = AllocDosObject(DOS_FIB, NULL);
    DOS_S32 file_size = 0xffffffff;

    if(fib)
    {
        if(file)
        {
            if(ExamineFH(file, fib))
            {
                file_size = fib->fib_Size;
            }
            else
            {
               dbg_printf("system_get_file_size: error\n");
            }
        }
        else
        {
            dbg_printf("system_get_file_size: no file handle\n");
        }

        FreeDosObject(DOS_FIB, fib);
    }

    return file_size;
}

/* /// */

