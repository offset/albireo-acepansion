## Version $VER: albireo.acepansion.catalog 1.4 (21.05.2021)
## Languages english fran�ais deutsch espa�ol
## Codeset english 0
## Codeset fran�ais 0
## Codeset deutsch 0
## Codeset espa�ol 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "generated/locale_strings.h" NoCode
## TARGET CATALOG fran�ais "Release/Catalogs/fran�ais/" Optimize
## TARGET CATALOG deutsch "Release/Catalogs/deutsch/" Optimize
## TARGET CATALOG espa�ol "Release/Catalogs/espa�ol/" Optimize
MSG_MENU_TOGGLE
Plug Albireo
Brancher une Albireo
Albireo anschlie�en
Conectar un Albireo
;
MSG_MENU_PREFS
Albireo configuration...
Configuration de l'Albireo...
Albireo-Konfiguration ...
Configuraci�n del Albireo...
;
MSG_TITLE
Albireo
Albireo
Albireo
Albireo
;
MSG_DRIVES
Drives configuration
Configuration des lecteurs
Konfiguration der Laufwerke
Configuraci�n de los lectores
;
MSG_SDCARD_DRIVE
Micro SD Card:
Carte MicroSD :
microSD-Karte:
Tarjeta MicroSD:
;
MSG_USB_DRIVE
USB port:
Port USB :
USB-Port:
Puerto USB:
;
MSG_ASL_REQUEST
Select the directory to use...
S�lectionnez le r�pertoire � utiliser...
W�hle das zu verwendende Verzeichnis aus ...
Elija la carpeta a usar...
;
MSG_SDCARD_DRIVE_HELP
Directory to use to simulate the Micro SD Card connected to the Albireo.
R�pertoire � utiliser pour simuler la carte Micro SD connect�e � l'Albireo.
Verzeichnis, das zur Simulation der mit dem Albireo verbundenen microSD-Karte verwendet werden soll.
Carpeta a utilizar para la emular la tarjeta Micro SD conectada al Albireo.
;
MSG_USB_DRIVE_HELP
Directory to use to simulate the USB device connected to the Albireo.
R�pertoire � utiliser pour simuler le p�riph�rique USB connect� � l'Albireo.
Verzeichnis, mit dem das am Albireo angeschlossene USB-Ger�t simuliert werden kann.
Carpeta a utilizar para emular un perif�rico USB conectado al Albireo.
;
